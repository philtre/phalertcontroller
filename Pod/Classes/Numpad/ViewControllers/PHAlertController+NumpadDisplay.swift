//
//  PHAlertController+NumpadDisplay.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-15.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    internal func createNumpadDisplayView() -> UIView {
        
        //MARK: metrics
        
        let minHeight = self.delegate?.alertControllerNumpadDisplayMinimumHeight?(self) ?? self.alertControllerNumpadDisplayMinimumHeight(self)
        
        let padding = self.delegate?.alertControllerNumpadDisplayPadding?(self) ?? self.alertControllerNumpadDisplayPadding(self)
        
        let spacing = self.delegate?.alertControllerNumpadDisplaySpacing?(self) ?? self.alertControllerNumpadDisplaySpacing(self)
        
        let labelMargins = self.delegate?.alertControllerNumpadDisplayLabelMargins?(self) ?? self.alertControllerNumpadDisplayLabelMargins(self)
        
        let labelPadding = self.delegate?.alertControllerNumpadDisplayLabelPadding?(self) ?? self.alertControllerNumpadDisplayLabelPadding(self)
        
        let clearAllBtnWidth = self.delegate?.alertControllerNumpadClearAllButtonWidth?(self) ?? self.alertControllerNumpadClearAllButtonWidth(self)
        
        let removeLastDigitBtnWidth = self.delegate?.alertControllerNumpadRemoveLastDigitButtonWidth?(self) ?? self.alertControllerNumpadRemoveLastDigitButtonWidth(self)
        
        let clearAllBtnMargins = self.delegate?.alertControllerNumpadClearAllButtonMargins?(self) ?? self.alertControllerNumpadClearAllButtonMargins(self)
        
        let removeLastDigitBtnMargins = self.delegate?.alertControllerNumpadRemoveLastDigitButtonMargins?(self) ?? self.alertControllerNumpadRemoveLastDigitButtonMargins(self)
        
        
        //MARK: views
        
        let containerView = self.delegate?.alertControllerNumpadDisplayContainerView?(self) ?? self.alertControllerNumpadDisplayContainerView(self)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        let labelBackView = self.delegate?.alertControllerNumpadDisplayLabelBackgroundView?(self) ?? self.alertControllerNumpadDisplayLabelBackgroundView(self)
        labelBackView.translatesAutoresizingMaskIntoConstraints = false
        
        
        let label = self.delegate?.alertControllerNumpadDisplayLabel?(self) ?? self.alertControllerNumpadDisplayLabel(self)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        _numpad?.displayLabel = label
        
        let clearAllBtn = self.delegate?.alertControllerNumpadClearAllButton?(self) ?? self.alertControllerNumpadClearAllButton(self)
        clearAllBtn.translatesAutoresizingMaskIntoConstraints = false
        
        
        let removeLastDigitBtn = self.delegate?.alertControllerNumpadRemoveLastDigitButton?(self) ?? self.alertControllerNumpadRemoveLastDigitButton(self)
        removeLastDigitBtn.translatesAutoresizingMaskIntoConstraints = false
        
        clearAllBtn.addTarget(self, action: "onPressNumpadClearAll:", forControlEvents: .TouchUpInside)
        
        removeLastDigitBtn.addTarget(self, action: "onPressNumpadRemoveLastDigit:", forControlEvents: .TouchUpInside)
        
        
        containerView.addSubview(labelBackView)
        containerView.addSubview(label)
        containerView.addSubview(clearAllBtn)
        containerView.addSubview(removeLastDigitBtn)
        
        
        // MARK: constraints
        
        let labelBackViewHeightConstraint = labelBackView.heightAnchor.constraintGreaterThanOrEqualToConstant(minHeight)
        
        let clearAllBtnWidthConstraint = clearAllBtn.widthAnchor.constraintEqualToConstant(clearAllBtnWidth)
        
        let removeLastDigitBtnWidthConstraint = removeLastDigitBtn.widthAnchor.constraintEqualToConstant(removeLastDigitBtnWidth)
        
        labelBackViewHeightConstraint.active = true
        
        for constraint in [clearAllBtnWidthConstraint, removeLastDigitBtnWidthConstraint] {
            
//            constraint.priority = 999
            constraint.active = true
            
        }
        
        
        let subviews = [clearAllBtn, labelBackView, removeLastDigitBtn]
        let subviewMargins = [clearAllBtnMargins, labelMargins, removeLastDigitBtnMargins]
        
        self.setupHorizontalConstraintsInContainer(containerView, padding: padding, subviews: subviews, subviewMargins: subviewMargins, spacing: spacing)
        
        
        self.setupConstraintsInContainer(labelBackView, subview: label, subviewMargins: labelPadding)
        
        
        
//        let views:[UIView] = [clearAllBtn, labelBackView, removeLastDigitBtn]
//        for view in views {
//            view.translatesAutoresizingMaskIntoConstraints = false
//            containerView.addSubview(view)
//            view.topAnchor.constraintEqualToAnchor(containerView.topAnchor).active = true
//            view.bottomAnchor.constraintEqualToAnchor(containerView.bottomAnchor).active = true
//        }
//        
//        views.first!.leadingAnchor.constraintEqualToAnchor(containerView.leadingAnchor, constant: margins.left).active = true
//        
//        for i in 1..<views.count {
//            views[i].leadingAnchor.constraintEqualToAnchor(views[i-1].trailingAnchor, constant: spacing.width).active = true
//        }
//        
//        containerView.trailingAnchor.constraintEqualToAnchor(views.last!.trailingAnchor, constant: margins.right).active = true
        
        
//        clearBtn.widthAnchor.constraintEqualToConstant(self.numpadLabelRowButtonWidth()).active = true
//        deleteBtn.widthAnchor.constraintEqualToConstant(self.numpadLabelRowButtonWidth()).active = true
        
        
//        let mh:CGFloat = 8.0
//        let mv:CGFloat = 0.0
//        let labelMargins = UIEdgeInsets(top: mv, left: mh, bottom: mv, right: mh)
//        
//        let labelLeadingConstraint = label.leadingAnchor.constraintEqualToAnchor(labelBackView.leadingAnchor, constant: labelMargins.left)
//        
//        let labelTrailingConstraint = label.trailingAnchor.constraintEqualToAnchor(labelBackView.trailingAnchor, constant: -labelMargins.right)
//        
//        let labelTopConstraint = label.topAnchor.constraintEqualToAnchor(labelBackView.topAnchor, constant: labelMargins.top)
//        
//        let labelBottomConstraint = label.bottomAnchor.constraintEqualToAnchor(labelBackView.bottomAnchor, constant: -labelMargins.bottom)
//        
//        for constraint in [labelLeadingConstraint, labelTrailingConstraint, labelTopConstraint, labelBottomConstraint] {
////            constraint.priority = 999
//            constraint.active = true
//        }
        
        
        
//        label.centerYAnchor.constraintEqualToAnchor(labelBackView.centerYAnchor).active = true
        
        
        return containerView
        
    }
    
//MARK: - Views -
    
    public func alertControllerNumpadDisplayContainerView(alertController: PHAlertController) -> UIView {
        let view = UIView()
//        view.backgroundColor = UIColor.cyanColor().colorWithAlphaComponent(0.5)
        return view
    }
    
    public func alertControllerNumpadDisplayLabelBackgroundView(alertController: PHAlertController) -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
//        view.backgroundColor = UIColor.cyanColor().colorWithAlphaComponent(0.5)
        return view
    }
    
    public func alertControllerNumpadDisplayLabel(alertController: PHAlertController) -> UILabel {
        
        let lbl:UILabel = UILabel()
        lbl.font = UIFont.boldSystemFontOfSize(UIFont.buttonFontSize() + 2.0)
        lbl.backgroundColor = UIColor.clearColor()
//        lbl.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.5)
        lbl.textColor = UIColor.blackColor()
        lbl.textAlignment = .Right
        lbl.lineBreakMode = .ByClipping
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.1
        return lbl
    }
    
    public func alertControllerNumpadClearAllButton(alertController: PHAlertController) -> UIButton {
        return PHAlertControllerNumpadDisplayButton(title: "X")
    }
    public func alertControllerNumpadRemoveLastDigitButton(alertController: PHAlertController) -> UIButton {
        return PHAlertControllerNumpadDisplayButton(title: "<")
    }
    
//MARK: - Metrics -
    
    public func alertControllerNumpadDisplayMinimumHeight(alertController:PHAlertController) -> CGFloat {
        return 44.0
    }
    
    private var numpadDisplayButtonWidth:CGFloat {
        return 44.0
    }
    public func alertControllerNumpadClearAllButtonWidth(alertController:PHAlertController) -> CGFloat {
        return self.numpadDisplayButtonWidth
    }
    public func alertControllerNumpadRemoveLastDigitButtonWidth(alertController:PHAlertController) -> CGFloat {
        return self.numpadDisplayButtonWidth
    }
    
    public func alertControllerNumpadDisplayMargins(alertController:PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        let b:CGFloat = 1.0
        return UIEdgeInsets(top: m, left: m, bottom: b, right: m)
    }
    public func alertControllerNumpadDisplayPadding(alertController:PHAlertController) -> UIEdgeInsets {
        let p:CGFloat = 0.0
        return UIEdgeInsets(top: p, left: p, bottom: p, right: p)
    }
    public func alertControllerNumpadDisplaySpacing(alertController:PHAlertController) -> CGFloat {
        return 1.0
    }
    public func alertControllerNumpadDisplayLabelMargins(alertController:PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
    }
    public func alertControllerNumpadDisplayLabelPadding(alertController:PHAlertController) -> UIEdgeInsets {
        let mv:CGFloat = 0.0
        let mh:CGFloat = 8.0
        return UIEdgeInsets(top: mv, left: mh, bottom: mv, right: mh)
    }
    public func alertControllerNumpadClearAllButtonMargins(alertController:PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
    }
    public func alertControllerNumpadRemoveLastDigitButtonMargins(alertController:PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
    }
    
//MARK: - Button Handlers -
    
    @objc private func onPressNumpadClearAll(btn:UIButton) {
        _numpad?.clear()
        self.updateNumpadDisplay()
    }
    
    @objc private func onPressNumpadRemoveLastDigit(btn:UIButton) {
        _numpad?.removeDigit()
        self.updateNumpadDisplay()
    }
    
}