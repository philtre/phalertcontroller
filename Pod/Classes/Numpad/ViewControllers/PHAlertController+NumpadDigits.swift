//
//  PHAlertController+NumpadDigits.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-15.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    public func createNumpadDigitButtonsView() -> UIView {
        
        
        let reversed = false
        
        let spacing = self.delegate?.alertControllerNumpadDigitSpacing?(self) ?? self.alertControllerNumpadDigitSpacing(self)
        
        
        let zeroMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let stack123 = self.createNumpadDigitButtonsRowForRange(1...3, reversed:reversed, margins:zeroMargins, spacing: spacing)
        let stack456 = self.createNumpadDigitButtonsRowForRange(4...6, reversed:reversed, margins:zeroMargins, spacing: spacing)
        let stack789 = self.createNumpadDigitButtonsRowForRange(7...9, reversed:reversed, margins:zeroMargins, spacing: spacing)
        let stack0 = self.createNumpadDigitButtonsRowForRange(0...0, reversed:reversed, margins:zeroMargins, spacing: spacing)
        
        let toggleNegativeBtn:UIButton
        let decimalPointBtn:UIButton
        
        if _numpad?.allowNegativeValues ?? false {
            toggleNegativeBtn = self.delegate?.alertControllerNumpadToggleNegativeButton?(self) ?? self.alertControllerNumpadToggleNegativeButton(self)
            toggleNegativeBtn.addTarget(self, action: "onPressNumpadToggleNegative:", forControlEvents: .TouchUpInside)
        } else {
            toggleNegativeBtn = self.delegate?.alertControllerNumpadBottomLeftButton?(self) ?? self.alertControllerNumpadBottomLeftButton(self)
        }
        if _numpad?.allowDecimalValues ?? false {
            let decimalPointSymbol = _numpad!.decimalPointSymbol
            decimalPointBtn = self.delegate?.alertController?(self, numpadButtonForDecimalPointWithSymbol: decimalPointSymbol) ?? self.alertController(self, numpadButtonForDecimalPointWithSymbol: decimalPointSymbol)
            decimalPointBtn.addTarget(self, action: "onPressNumpadDecimalPoint:", forControlEvents: .TouchUpInside)
        } else {
            decimalPointBtn = self.delegate?.alertControllerNumpadBottomRightButton?(self) ?? self.alertControllerNumpadBottomRightButton(self)
        }
        stack0.insertArrangedSubview(toggleNegativeBtn, atIndex:0)
        stack0.insertArrangedSubview(decimalPointBtn, atIndex:2)
        
        var rows:[UIView] = [stack123,stack456,stack789]
        
        switch _numpad!.direction {
            case .TopDown:
                break
//                rows.append(stack123)
//                rows.append(stack456)
//                rows.append(stack789)
            case .BottomUp:
                rows = rows.reverse()
//                rows.append(stack789)
//                rows.append(stack456)
//                rows.append(stack123)
        }
        rows.append(stack0)
        
        
        let stack = UIStackView(arrangedSubviews: rows)
        
        let padding = self.delegate?.alertControllerNumpadDigitsPadding?(self) ?? self.alertControllerNumpadDigitsPadding(self)
        
//        stack.layoutMargins = padding
        stack.layoutMarginsRelativeArrangement = false
        stack.alignment = .Fill
        stack.axis = .Vertical
        stack.distribution = .FillEqually
        stack.spacing = spacing.height
        
        let containerView = self.delegate?.alertControllerNumpadDigitsContainerView?(self) ?? self.alertControllerNumpadDigitsContainerView(self)
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(stack)
        
        
        self.setupConstraintsInContainer(containerView, subview: stack, subviewMargins: padding)
        
        
        return containerView
//        return stack
        
    }
    
    
    public func createNumpadDigitButtonsRowForRange(range:Range<Int>, reversed:Bool, margins:UIEdgeInsets, spacing:CGSize) -> UIStackView {
        
        var buttons = [UIView]()
        
        for i in range {
            
            let btn = self.delegate?.alertController?(self, numpadButtonForDigit: i) ?? self.alertController(self, numpadButtonForDigit: i)
            btn.addTarget(self, action: "onPressNumpadDigit:", forControlEvents: .TouchUpInside)
            buttons.append(btn)
            _numpad?.addButton(btn, forDigit: i)
        }
        
        if reversed {
            buttons = buttons.reverse()
        }
        
        let stack = UIStackView(arrangedSubviews: buttons)
        
        stack.layoutMargins = margins
        stack.layoutMarginsRelativeArrangement = true
        stack.alignment = .Fill
        stack.axis = .Horizontal
        stack.distribution = .FillEqually
        stack.spacing = spacing.width
        
        return stack
    }
    
    public func alertControllerNumpadDigitsMargins(alertController: PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
    }
    public func alertControllerNumpadDigitsPadding(alertController: PHAlertController) -> UIEdgeInsets {
        let p:CGFloat = 0.0
        return UIEdgeInsets(top: p, left: p, bottom: p, right: p)
    }
    
    public func alertControllerNumpadDigitSpacing(alertController: PHAlertController) -> CGSize {
        let s:CGFloat = 1.0
        return CGSize(width: s, height: s)
    }
    
    public func alertControllerNumpadDigitsContainerView(alertController: PHAlertController) -> UIView {
        let view = UIView()
//        view.backgroundColor = UIColor.greenColor().colorWithAlphaComponent(0.5)
        return view
    }
    
    public func alertController(alertController: PHAlertController, numpadButtonForDigit digit: Int) -> UIButton {
        return PHAlertControllerNumpadDigitButton(digit: digit)
    }
    
    public func alertControllerNumpadToggleNegativeButton(alertController: PHAlertController) -> UIButton {
        return PHAlertControllerNumpadDigitButton(title: "+/-")
    }
    public func alertController(alertsController: PHAlertController, numpadButtonForDecimalPointWithSymbol decimalPointSymbol: String) -> UIButton {
        return PHAlertControllerNumpadDigitButton(title: decimalPointSymbol)
    }
    
    public func alertControllerNumpadBottomLeftButton(alertController: PHAlertController) -> UIButton {
        let btn = PHAlertControllerNumpadDigitButton(title: nil)
        btn.enabled = false
        return btn
    }
    public func alertControllerNumpadBottomRightButton(alertController: PHAlertController) -> UIButton {
        let btn = PHAlertControllerNumpadDigitButton(title: nil)
        btn.enabled = false
        return btn
    }
    
    @objc private func onPressNumpadDigit(btn:UIButton) {
        guard let digit = _numpad?.digitForButton(btn) else {
            return
        }
        _numpad?.addDigit(digit)
        self.updateNumpadDisplay()
    }
    
    @objc private func onPressNumpadToggleNegative(btn:UIButton) {
        _numpad?.toggleNegative()
        self.updateNumpadDisplay()
    }
    
    @objc private func onPressNumpadDecimalPoint(btn:UIButton) {
        _numpad?.enableDecimalMode()
        self.updateNumpadDisplay()
    }
    
}