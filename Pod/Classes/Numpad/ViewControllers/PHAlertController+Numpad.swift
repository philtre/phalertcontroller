//
//  PHAlertController+CreateNumpad.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    public var hasNumpad:Bool {
        return _numpad != nil
    }
    
    static public var defaultNumpadFormatter:NSNumberFormatter {
        return PHAlertControllerNumpad.defaultNumberFormatter
    }
    
    public func setNumpadValue(value:Int) {
        _numpad?.setValue(value)
        self.updateNumpadDisplay()
    }
    
    public func setNumpadValue(value:Double, precision:UInt?) {
        _numpad?.setValue(value, precision: precision)
        self.updateNumpadDisplay()
    }
    
    public var numpadIntegerValue:Int? {
        get {
            return _numpad?.integerValue
        }
        set {
            if let value = newValue {
                _numpad?.integerValue = value
                self.updateNumpadDisplay()
            }
        }
    }
    
    public var numpadDoubleValue:Double? {
        get {
            return _numpad?.doubleValue
        }
        set {
            if let value = newValue {
                _numpad?.doubleValue = value
                self.updateNumpadDisplay()
            }
        }
    }
    
    
    internal func createNumpadViewForStyle(style:PHAlertControllerStyle) -> UIView? {
        if !self.hasNumpad {
            return nil
        }
        
//        let labelsMargins:UIEdgeInsets = self.delegate?.alertControllerMarginsForLabels?(self) ?? self.alertControllerMarginsForLabels(self)
//        let labelSpacing:CGFloat = self.delegate?.alertControllerSpacingForLabels?(self) ?? self.alertControllerSpacingForLabels(self)
//        
//        let buttonsMargins:UIEdgeInsets = self.delegate?.alertControllerMarginsForButtons?(self) ?? self.alertControllerMarginsForButtons(self)
//        let buttonSpacing:CGSize = self.delegate?.alertControllerSpacingForButtons?(self) ?? self.alertControllerSpacingForButtons(self)
//        
//        
//        let reversed = false
//        let zeroMargins = UIEdgeInsets()
        
        let margins = self.delegate?.alertControllerNumpadMargins?(self) ?? self.alertControllerNumpadMargins(self)
        let padding = self.delegate?.alertControllerNumpadPadding?(self) ?? self.alertControllerNumpadPadding(self)
        
        let displayMargins = self.delegate?.alertControllerNumpadDisplayMargins?(self) ?? self.alertControllerNumpadDisplayMargins(self)
        
        let digitsMargins = self.delegate?.alertControllerNumpadDigitsMargins?(self) ?? self.alertControllerNumpadDigitsMargins(self)
        
        let numpadView = self.delegate?.alertControllerNumpadContainerView?(self) ?? self.alertControllerNumpadContainerView(self)
        
        
        
        let displayView = self.createNumpadDisplayView()
        
        let digitButtonsView = self.createNumpadDigitButtonsView()
        
        let subviews:[UIView] = [displayView, digitButtonsView]
        let subviewMargins:[UIEdgeInsets] = [displayMargins, digitsMargins]
        
        self.setupVerticalConstraintsInContainer(numpadView, padding: padding, subviews: subviews, subviewMargins: subviewMargins)
        
        
        let containerView = UIView()
        self.setupConstraintsInContainer(containerView, subview: numpadView, subviewMargins: margins)
        
        
//        let stack = UIStackView(arrangedSubviews: views)
//        
//        stack.layoutMargins = zeroMargins
//        stack.layoutMarginsRelativeArrangement = true
//        stack.alignment = .Fill
//        stack.axis = .Vertical
//        stack.distribution = .EqualSpacing
//        stack.spacing = 0
        
        self.updateNumpadDisplay()
        
        return containerView
    }
    
    
    public func alertControllerNumpadDirection(alertController:PHAlertController) -> PHAlertControllerNumpadDirection {
        return .BottomUp
    }
    public func alertControllerNumpadAllowsDecimalValues(alertController:PHAlertController) -> Bool {
        return true
    }
    public func alertControllerNumpadAllowsNegativeValues(alertController:PHAlertController) -> Bool {
        return true
    }
    public func alertControllerNumpadDisplayFormatter(alertController:PHAlertController) -> NSNumberFormatter? {
        return nil
    }
    public func alertControllerNumpadDisplayPrefix(alertController:PHAlertController) -> String {
        return ""
    }
    public func alertControllerNumpadDisplaySuffix(alertController:PHAlertController) -> String {
        return ""
    }
    
    public func alertControllerNumpadMargins(alertController: PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
    }
    public func alertControllerNumpadPadding(alertController: PHAlertController) -> UIEdgeInsets {
        let p:CGFloat = 0.0
        return UIEdgeInsets(top: p, left: p, bottom: p, right: p)
    }
    
    
    
    
    
//    public func createNumpadButtonWithDigit(number:Int) -> UIButton {
//        let btn = self.createNumpadButtonWithTitle("\(number)")
//        btn.addTarget(self, action: "onPressNumber:", forControlEvents: .TouchUpInside)
//        return btn
//    }
    
    
//    public func createNumpadButtonWithTitle(title:String?) -> UIButton {
//        
//        let btn = self.createButtonWithTitle(title, actionStyle: .Cancel, controllerStyle: .Alert)
//        btn.backgroundColor = UIColor.orangeColor()
//        
//        return btn
//    }
    
    internal func updateNumpadDisplay() {
        guard let numpad = _numpad, displayLabel = numpad.displayLabel else {
            return
        }
        
        displayLabel.text = numpad.displayString
        print(numpad.debugDescription)
        
        
    }
    
    public func alertControllerNumpadContainerView(alertController: PHAlertController) -> UIView {
        let view = UIView()
//        view.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.5)
        return view
    }
    
    
    
    
    
    
}

