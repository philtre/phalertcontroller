//
//  PHAlertControllerNumpad.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-11.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

@objc public enum PHAlertControllerNumpadDirection: Int {
    case TopDown
    case BottomUp
}

internal class PHAlertControllerNumpad: CustomStringConvertible, CustomDebugStringConvertible {
    
    internal var direction:PHAlertControllerNumpadDirection = .BottomUp
    internal var allowNegativeValues:Bool = true
    internal var allowDecimalValues:Bool = true
    internal var defaultDoublePrecision:UInt = 4
    internal var displayPrefix:String = ""
    internal var displaySuffix:String = ""
    internal var displayLabel:UILabel?
    private var _digitsByButton = [UIButton:Int]()
    
    private var _mantissaLength:UInt = 0
    private var _mantissa:UInt64 = 0
    private var _exponent:Int16 = 0
    internal var negative:Bool = false
    internal private(set) var decimalMode:Bool = false
    
    private var _numberFormatter:NSNumberFormatter
    internal var numberFormatter:NSNumberFormatter { return _numberFormatter.copy() as! NSNumberFormatter }
    
    internal var decimalPointSymbol:String {
        return _numberFormatter.decimalSeparator
    }
    
    init(numberFormatter:NSNumberFormatter?) {
        
        _numberFormatter = numberFormatter ?? PHAlertControllerNumpad.defaultNumberFormatter
        
    }
    
    static var defaultNumberFormatter:NSNumberFormatter {
        let f = NSNumberFormatter()
        f.formatterBehavior = .Behavior10_4
        f.numberStyle = .DecimalStyle
        f.maximumFractionDigits = 100
        return f
    }
    
    internal func enableDecimalMode() {
        self.decimalMode = true
    }
    
    internal func addDigit(d:Int) {
        assert((0...9).contains(d), "Digit must be contained in interval 0...9")
        if _mantissaLength >= 20 {
            return
        }
        _mantissa = _mantissa * 10 + UInt64(d)
        _mantissaLength++
        
        if self.decimalMode {
            _exponent--
        }
    }
    
    internal func removeDigit() {
        if self.decimalMode && _exponent == 0 {
            self.decimalMode = false
            return
        }
        
        if _mantissa == 0 {
            return
        }
        
        _mantissa /= 10
        _mantissaLength--
        
        if _exponent < 0 {
            _exponent++
        }
    }
    
    internal func clear() {
        _mantissa = 0
        _exponent = 0
        _mantissaLength = 0
        self.negative = false
        self.decimalMode = false
    }
    
    internal func toggleNegative() {
        self.negative = !self.negative
    }
    
    internal var decimalNumber : NSDecimalNumber {
        let negative = _mantissa == 0 ? false : self.negative
        return NSDecimalNumber(mantissa: _mantissa, exponent: _exponent, isNegative: negative)
    }
    
    internal var doubleValue : Double {
        get {
            return self.decimalNumber.doubleValue
        }
        set {
            self.setValue(newValue)
        }
    }
    
    internal var integerValue : Int {
        get {
            return self.decimalNumber.integerValue
        }
        set {
            self.setValue(newValue)
        }
    }
    
    internal func setValue(value:Double, precision:UInt? = nil) {
        
        if value == 0 {
            self.clear()
            return
        }
        
        let absValue:Double = abs(value)
        
        let valueExponent = Int16(log10(absValue))
        
        if valueExponent >= 20 {
            self.clear()
            return
        }
        
        var actualPrecision:Int16 = Int16(precision ?? self.defaultDoublePrecision)
        actualPrecision = min(actualPrecision + valueExponent, 19) - valueExponent
        actualPrecision = max(actualPrecision, 0)
        
        
        var mantissa = UInt64(round(absValue * pow(10, Double(actualPrecision))))
        
        var exponent = -actualPrecision
        
        while mantissa > 0 && exponent < 0 && (mantissa % 10) == 0 {
            mantissa /= 10
            exponent++
        }
        
        _mantissa = mantissa
        _exponent = exponent
        _mantissaLength = UInt(valueExponent - exponent + 1)
        
        
    }
    
    internal func addButton(button:UIButton, forDigit digit:Int) {
        _digitsByButton[button] = digit
    }
    internal func digitForButton(button:UIButton) -> Int? {
        return _digitsByButton[button]
    }
    
    internal func setValue(value:Int) {
        if value == 0 {
            self.clear()
            return
        }
        let absValue = abs(value)
        _mantissa = UInt64(absValue)
        _exponent = 0
        
        let valueExponent = UInt(log10(Double(absValue)))
        _mantissaLength = valueExponent + 1
        
        self.negative = value < 0
        self.decimalMode = false
    }
    
    internal var description : String {
        
        let minFrac = _numberFormatter.minimumFractionDigits
        
        if self.decimalMode {
            let frac = max(1, Int(-_exponent))
            _numberFormatter.minimumFractionDigits = max(frac, minFrac)
        }
        
        let str = _numberFormatter.stringFromNumber(self.decimalNumber) ?? ""
        
        _numberFormatter.minimumFractionDigits = minFrac
        
        return str
    }
    
    internal var displayString : String {
        return "\(self.displayPrefix)\(self.description)\(self.displaySuffix)"
    }
    
    internal var debugDescription : String {
        return "(mantissa: \(_mantissa), exponent: \(_exponent), negative: \(self.negative), description: '\(self.description)')"
    }
    
    
}


/*
public enum PHNumpadAlertControllerNumpadDirection {
    case TopDown
    case BottomUp
}

public class PHNumpadAlertController : PHAlertController {
    
    public var numpadDirection:PHNumpadAlertControllerNumpadDirection = .BottomUp
    public var numpadReversed:Bool = false
    public var allowNegativeValues:Bool = true
    public var allowDecimalValues:Bool = true
    public var precision:Int = 4
    public var numpadStringPrefix:String = ""
    public var numpadStringSuffix:String = ""
    public var decimalPointSymbol:String = "."
    public var numberGroupingSymbol:String? = ","
//    private var digits = [PHNumpadAlertControllerDigit]()
    private var hasDecimalPoint:Bool = false
    private var isNegative:Bool = false
    private var numpadLabel:UILabel?
    
    private var integerDigits = [UInt64]()
    private var decimalDigits = [UInt64]()
    
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.updateNumpadLabel()
    }
    
    override public func createButtonsViewForStyle(style: PHAlertControllerStyle, margins:UIEdgeInsets, spacing:CGSize) -> UIView? {
        
        var views = [UIView]()
        
        let zeroMargins = UIEdgeInsets()
        
        if let view = self.createNumpadLabelRow(margins: zeroMargins, spacing: spacing) {
            views.append(view)
        }
        
        if let view = self.createNumpadButtonsView(numpadDirection: self.numpadDirection, reversed:self.numpadReversed, margins:zeroMargins, spacing:spacing) {
            views.append(view)
        }
        
        if let view = super.createButtonsViewForStyle(style, margins:zeroMargins, spacing:spacing) {
            views.append(view)
        }
        
        if views.isEmpty {
            return nil
        }
        
        let stack = UIStackView(arrangedSubviews: views)
        
        stack.layoutMargins = margins
        stack.layoutMarginsRelativeArrangement = true
        stack.alignment = .Fill
        stack.axis = .Vertical
        stack.distribution = .EqualSpacing
        stack.spacing = spacing.height
        
        return stack
        
    }
    
    public func createNumpadLabelRow(margins margins:UIEdgeInsets, spacing:CGSize) -> UIView? {
        let lblBack = UIView()
        lblBack.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.9)
        
        let lbl = self.createNumpadLabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        self.numpadLabel = lbl
        
        let clearBtn = self.createClearButton()
        let deleteBtn = self.createDeleteButton()
        
        clearBtn.addTarget(self, action: "onPressClear:", forControlEvents: .TouchUpInside)
        deleteBtn.addTarget(self, action: "onPressDelete:", forControlEvents: .TouchUpInside)
        
        
        let stack = UIView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        
        let views:[UIView] = [clearBtn, lblBack, deleteBtn]
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            stack.addSubview(view)
//            view.topAnchor.constraintEqualToAnchor(stack.topAnchor).active = true
//            view.bottomAnchor.constraintEqualToAnchor(stack.bottomAnchor).active = true
        }
        
//        views.first!.leadingAnchor.constraintEqualToAnchor(stack.leadingAnchor, constant: margins.left).active = true
        
        for i in 1..<views.count {
//            views[i].leadingAnchor.constraintEqualToAnchor(views[i-1].trailingAnchor, constant: spacing.width).active = true
        }
        
//        stack.trailingAnchor.constraintEqualToAnchor(views.last!.trailingAnchor, constant: margins.right).active = true
        
        
//        clearBtn.widthAnchor.constraintEqualToConstant(self.numpadLabelRowButtonWidth()).active = true
//        deleteBtn.widthAnchor.constraintEqualToConstant(self.numpadLabelRowButtonWidth()).active = true
        
        stack.addSubview(lbl)
        
        let labelMargin:CGFloat = 8.0
//        lbl.leadingAnchor.constraintEqualToAnchor(lblBack.leadingAnchor, constant: labelMargin).active = true
//        lbl.trailingAnchor.constraintEqualToAnchor(lblBack.trailingAnchor, constant: -labelMargin).active = true
//        lbl.centerYAnchor.constraintEqualToAnchor(lblBack.centerYAnchor).active = true
        
        
        return stack
        
    }
    
    public func numpadLabelRowButtonWidth() -> CGFloat {
        return 44.0
    }
    
    public func createNumpadLabel() -> UILabel {
        
        let lbl:UILabel = UILabel()
        lbl.font = UIFont.boldSystemFontOfSize(22.0)
        lbl.backgroundColor = UIColor.clearColor()
        lbl.textColor = UIColor.blackColor()
        lbl.textAlignment = .Right
        lbl.lineBreakMode = .ByClipping
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.1
        return lbl
    }
    public func createClearButton() -> UIButton {
        return self.createNumpadButtonWithTitle("X")
    }
    public func createDeleteButton() -> UIButton {
        return self.createNumpadButtonWithTitle("<")
    }
    public func createNegativeToggleButton() -> UIButton {
        return self.createNumpadButtonWithTitle("+/-")
    }
    public func createDecimalPointButton() -> UIButton {
        return self.createNumpadButtonWithTitle(self.decimalPointSymbol)
    }
    
    
    public func createNumpadButtonsView(numpadDirection numpadDirection:PHNumpadAlertControllerNumpadDirection, reversed:Bool, margins:UIEdgeInsets, spacing:CGSize) -> UIView? {
        
        let zeroMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let stack123 = self.createNumpadRowButtonsView(range: 1...3, reversed:reversed, margins:zeroMargins, spacing: spacing)
        let stack456 = self.createNumpadRowButtonsView(range: 4...6, reversed:reversed, margins:zeroMargins, spacing: spacing)
        let stack789 = self.createNumpadRowButtonsView(range: 7...9, reversed:reversed, margins:zeroMargins, spacing: spacing)
        let stack0 = self.createNumpadRowButtonsView(range: 0...0, reversed:reversed, margins:zeroMargins, spacing: spacing)
        
        let toggleNegativeBtn:UIButton
        let decimalPointBtn:UIButton
        
        if self.allowNegativeValues {
            toggleNegativeBtn = self.createNegativeToggleButton()
            toggleNegativeBtn.addTarget(self, action: "onPressToggleNegative:", forControlEvents: .TouchUpInside)
        } else {
            toggleNegativeBtn = self.createNumpadButtonWithTitle(nil)
        }
        if self.allowDecimalValues {
            decimalPointBtn = self.createNumpadButtonWithTitle(".")
            decimalPointBtn.addTarget(self, action: "onPressDecimalPoint:", forControlEvents: .TouchUpInside)
        } else {
            decimalPointBtn = self.createNumpadButtonWithTitle(nil)
        }
        stack0.insertArrangedSubview(toggleNegativeBtn, atIndex:0)
        stack0.insertArrangedSubview(decimalPointBtn, atIndex:2)
        
        var rows = [UIView]()
        
        switch numpadDirection {
            case .TopDown:
                rows.append(stack123)
                rows.append(stack456)
                rows.append(stack789)
            case .BottomUp:
                rows.append(stack789)
                rows.append(stack456)
                rows.append(stack123)
        }
        rows.append(stack0)
        
        
        let stack = UIStackView(arrangedSubviews: rows)
        
        stack.layoutMargins = margins
        stack.layoutMarginsRelativeArrangement = true
        stack.alignment = .Fill
        stack.axis = .Vertical
        stack.distribution = .FillEqually
        stack.spacing = spacing.width
        
        return stack
        
    }
    
    public func createNumpadRowButtonsView(range range:Range<Int>, reversed:Bool, margins:UIEdgeInsets, spacing:CGSize) -> UIStackView {
        
        var buttons = [UIView]()
        
        for i in range {
            
            let btn = self.createNumpadButtonWithNumber(i)
            buttons.append(btn)
            
        }
        
        if reversed {
            buttons = buttons.reverse()
        }
        
        let stack = UIStackView(arrangedSubviews: buttons)
        
        stack.layoutMargins = margins
        stack.layoutMarginsRelativeArrangement = true
        stack.alignment = .Fill
        stack.axis = .Horizontal
        stack.distribution = .FillEqually
        stack.spacing = spacing.width
        
        return stack
    }
    
    public func createNumpadButtonWithNumber(number:Int) -> UIButton {
        let btn = self.createNumpadButtonWithTitle("\(number)")
        btn.addTarget(self, action: "onPressNumber:", forControlEvents: .TouchUpInside)
        return btn
    }
    
    
    public func createNumpadButtonWithTitle(title:String?) -> UIButton {
        
        let btn = self.createButtonWithTitle(title, actionStyle: .Cancel, controllerStyle: .Alert)
        
        return btn
    }

    
    @objc private func onPressNumber(btn:UIButton) {
        
        guard let title = btn.titleForState(.Normal) ?? btn.attributedTitleForState(.Normal)?.string, let number = UInt64(title) else {
            return
        }
        if self.hasDecimalPoint {
            if self.decimalDigits.count < 19 {
                self.decimalDigits.append(number)
            }
        } else {
            if !(number == 0 && self.integerDigits.isEmpty) && self.integerDigits.count < 19 {
                self.integerDigits.append(number)
            }
        }
        self.updateNumpadLabel()
        
    }
    @objc private func onPressDecimalPoint(btn:UIButton) {
        if hasDecimalPoint {
            return
        }
        self.hasDecimalPoint = true
        self.updateNumpadLabel()
    }
    @objc private func onPressToggleNegative(btn:UIButton) {
        self.isNegative = !self.isNegative
        self.updateNumpadLabel()
    }
    @objc private func onPressClear(btn:UIButton) {
        self.integerDigits.removeAll()
        self.decimalDigits.removeAll()
        self.isNegative = false
        self.hasDecimalPoint = false
        self.updateNumpadLabel()
    }
    @objc private func onPressDelete(btn:UIButton) {
        
        if self.hasDecimalPoint {
            
            if self.decimalDigits.isEmpty {
                self.hasDecimalPoint = false
            } else {
                self.decimalDigits.removeLast()
            }
        } else {
            if self.integerDigits.isEmpty {
                return
            } else {
                self.integerDigits.removeLast()
            }
        }
        
        self.updateNumpadLabel()
    }
    
    private func updateNumpadLabel() {
        
        print("intValue: \(self.intValue)")
        print("doubleValue: \(self.doubleValue)")
        
        self.numpadLabel?.text = self.numpadString
        
    }
    public var numpadString:String {
        return "\(self.numpadStringPrefix)\(self.numpadNumberString)\(self.numpadStringSuffix)"
    }
    public var numpadNumberString:String {
        
        
        var chars = [String]()
        
        if self.integerDigits.isEmpty {
            chars.append("0")
        } else {
            
            var intChars = self.integerDigits.map{ return "\($0)" }
            
            if let numberGroupingSymbol = self.numberGroupingSymbol where numberGroupingSymbol != "" {
                let stride = 3
                var i = intChars.count - stride
                while i > 0 {
                    intChars.insert(numberGroupingSymbol, atIndex: i)
                    i -= stride
                }
            }
            
            chars.appendContentsOf(intChars)
        }
        
        if self.hasDecimalPoint {
            chars.append(self.decimalPointSymbol)
            
            if self.decimalDigits.isEmpty {
                chars.append("0")
            } else {
                chars.appendContentsOf(self.decimalDigits.map{ return "\($0)" })
            }
        }
        
        if self.isNegative && (self.integerDigits.count + self.decimalDigits.count) > 0 {
            chars.insert("-", atIndex: 0)
        }
        
        return chars.joinWithSeparator("")
        
    }
    
    public var intValue:Int {
        
        get {
            let val:UInt64 = self.integerDigits.reduce(0) {
                return $0 * 10 + $1
            }
            
            return self.isNegative ? -Int(val) : Int(val)
        }
        set {
            if newValue == 0 {
                self.integerDigits.removeAll()
            } else {
                var val:UInt64 = UInt64(abs(newValue))
                
                var arr = [UInt64]()
                while val > 0 {
                    arr.append(val % 10)
                    val /= 10
                }
                self.integerDigits = arr.reverse()
            }
            
            self.decimalDigits.removeAll()
            
            self.isNegative = newValue < 0
            
            self.hasDecimalPoint = false
            
            self.updateNumpadLabel()
        }
    }
    public var doubleValue:Double {
        
        get {
            
            let intVal:UInt64 = self.integerDigits.reduce(0) {
                return $0 * 10 + $1
            }
            
            var f:UInt64 = 1
            let decVal:UInt64 = self.decimalDigits.reduce(0){
                let val = $0 * 10 + $1
                f *= 10
                return val
            }
            let val = Double(intVal) + Double(decVal)/Double(f)
            
            return self.isNegative ? -val : val
        }
        
        set {
            self.setDoubleValue(newValue, precision:self.precision)
        }
    }
    
    func setDoubleValue(newValue:Double, precision:Int) {
        
        if newValue == 0 {
            self.integerDigits.removeAll()
            self.decimalDigits.removeAll()
            self.isNegative = false
            self.hasDecimalPoint = true
            return
        }
        
        let val = abs(newValue)
        
        var intVal = UInt64(val)
        var decVal = UInt64(round(((val - Double(intVal)) * pow(10.0, Double(precision)))))
        
        var intArr = [UInt64]()
        var decArr = [UInt64]()
        
        
        while intVal > 0 {
            intArr.append(intVal%10)
            intVal /= 10
        }
        
        while decVal > 0 && decVal % 10 == 0 {
            decVal /= 10
        }
        while decVal > 0 {
            decArr.append(decVal%10)
            decVal /= 10
        }
        
        self.integerDigits = intArr.reverse()
        self.decimalDigits = decArr.reverse()
        
        self.hasDecimalPoint = true
        self.isNegative = newValue < 0
        
        self.updateNumpadLabel()
    
    }
}
*/