//
//  PHAlertControllerNumpadDigitButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerNumpadDigitButton : PHAlertControllerDefaultButton {
    
    internal convenience init(digit:Int) {
        self.init(title:"\(digit)")
    }
    internal convenience init(title:String?) {
        self.init(frame:.zero)
        self.setTitle(title, font: nil, color: nil, forState: .Normal)
    }
    
    override func setup() {
        super.setup()
        
        self.setBackgroundColor(UIColor.whiteColor().colorWithAlphaComponent(0.8), forState: .Normal)
    }
    
    override var minimumIntrinsicContentSize:CGSize {
        return CGSize(width: 0.0, height: 44.0)
    }
    
}
