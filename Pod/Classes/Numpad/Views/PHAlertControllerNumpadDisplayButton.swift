//
//  PHAlertControllerNumpadDisplayButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerNumpadDisplayButton : PHAlertControllerDefaultButton {
    
    internal convenience init(title:String?) {
        self.init(frame:.zero)
        self.setTitle(title, font: nil, color: nil, forState: .Normal)
    }
}
