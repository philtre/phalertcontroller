//
//  PHAlertController+LayoutHelpers.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    internal func setupHorizontalConstraintsInContainer(containerView:UIView, padding:UIEdgeInsets, subviews:[UIView], subviewMargins:[UIEdgeInsets], spacing:CGFloat = 0.0) {
        
        var verticalConstraints = [NSLayoutConstraint]()
        
        for i in 0..<subviews.count {
            let subview = subviews[i]
            subview.translatesAutoresizingMaskIntoConstraints = false
            if subview.superview == nil {
                containerView.addSubview(subview)
            }
            let margins = subviewMargins[i]
            let top = subview.topAnchor.constraintEqualToAnchor(containerView.topAnchor, constant: padding.top + margins.top)
            let bottom = containerView.bottomAnchor.constraintEqualToAnchor(subview.bottomAnchor, constant: margins.bottom + padding.bottom)
            verticalConstraints.append(top)
            verticalConstraints.append(bottom)
        }
        
        
        let leading = subviews.first!.leadingAnchor.constraintEqualToAnchor(containerView.leadingAnchor, constant: padding.left + subviewMargins.first!.left)
        
        let trailing = containerView.trailingAnchor.constraintEqualToAnchor(subviews.last!.trailingAnchor, constant: subviewMargins.last!.right + padding.right)
        
        var horizontalConstraints:[NSLayoutConstraint] = [leading, trailing]
        
        for i in 1..<subviews.count {
            let c = subviews[i].leadingAnchor.constraintEqualToAnchor(subviews[i-1].trailingAnchor, constant: subviewMargins[i-1].right + subviewMargins[i].left + spacing)
            
            horizontalConstraints.append(c)
        }
        
        
        let allConstraints = horizontalConstraints + verticalConstraints
        
        for constraint in allConstraints {
            //                constraint.priority = 999
            constraint.active = true
        }
    }
    internal func setupVerticalConstraintsInContainer(containerView:UIView, padding:UIEdgeInsets, subviews:[UIView], subviewMargins:[UIEdgeInsets], spacing:CGFloat = 0.0) {
        
        var horizontalConstraints = [NSLayoutConstraint]()
        
        for i in 0..<subviews.count {
            let subview = subviews[i]
            subview.translatesAutoresizingMaskIntoConstraints = false
            if subview.superview == nil {
                containerView.addSubview(subview)
            }
            let margins = subviewMargins[i]
            let leading = subview.leadingAnchor.constraintEqualToAnchor(containerView.leadingAnchor, constant: padding.left + margins.left)
            let trailing = containerView.trailingAnchor.constraintEqualToAnchor(subview.trailingAnchor, constant: margins.right + padding.right)
            horizontalConstraints.append(leading)
            horizontalConstraints.append(trailing)
        }
        
        let top = subviews.first!.topAnchor.constraintEqualToAnchor(containerView.topAnchor, constant: padding.top + subviewMargins.first!.top)
        
        let bottom = containerView.bottomAnchor.constraintEqualToAnchor(subviews.last!.bottomAnchor, constant: subviewMargins.last!.bottom + padding.bottom)
        
        var verticalConstraints:[NSLayoutConstraint] = [top, bottom]
        
        for i in 1..<subviews.count {
            let c = subviews[i].topAnchor.constraintEqualToAnchor(subviews[i-1].bottomAnchor, constant: subviewMargins[i-1].bottom + subviewMargins[i].top + spacing)
            
            verticalConstraints.append(c)
        }
        
        
        let allConstraints = horizontalConstraints + verticalConstraints
        
        for constraint in allConstraints {
            //                constraint.priority = 999
            constraint.active = true
        }
    }
    
    func setupConstraintsInContainer(containerView:UIView, subview:UIView, subviewMargins:UIEdgeInsets = UIEdgeInsets()) {
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        if subview.superview == nil {
            containerView.addSubview(subview)
        }
        
        let leading = subview.leadingAnchor.constraintEqualToAnchor(containerView.leadingAnchor, constant: subviewMargins.left)
        
        let trailing = containerView.trailingAnchor.constraintEqualToAnchor(subview.trailingAnchor, constant: subviewMargins.right)
        
        let top = subview.topAnchor.constraintEqualToAnchor(containerView.topAnchor, constant: subviewMargins.top)
        
        let bottom = containerView.bottomAnchor.constraintEqualToAnchor(subview.bottomAnchor, constant: subviewMargins.bottom)
        
        let allConstraints:[NSLayoutConstraint] = [leading, trailing, top, bottom]
        
        for constraint in allConstraints {
            //                constraint.priority = 999
            constraint.active = true
        }
        
        
        
    }
    
}