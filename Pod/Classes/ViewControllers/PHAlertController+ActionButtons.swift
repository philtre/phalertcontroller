//
//  PHAlertController+Buttons.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    internal func createButtonsViewForStyle(style:PHAlertControllerStyle) -> UIView? {
//        return nil
        var buttons = [UIView]()
        
        let actionCount = _actions.count
        
        var hasCancelAction = false
        let preferredAction = self.preferredAction
        let preferredActionExists:Bool
        
        if let preferredAction = preferredAction, _ = _actions.indexOf(preferredAction) {
            preferredActionExists = true
        } else {
            preferredActionExists = false
        }
        
        
        
        let distributeHorizontally = style == .Alert && actionCount <= 2
        
        for action in self.actions {
            
            let isPreferredAction = preferredActionExists && action == preferredAction
            
            let button = self.delegate?.alertController?(self, buttonForAction: action, isPreferredAction:isPreferredAction, preferredActionExists:preferredActionExists) ?? self.alertController(self, buttonForAction: action, isPreferredAction:isPreferredAction, preferredActionExists:preferredActionExists)
            
            
//            let button = self.createButtonForAction(action, style:style)
            button.translatesAutoresizingMaskIntoConstraints = false
//            button.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, forAxis: .Horizontal)
//            button.setContentHuggingPriority(UILayoutPriorityDefaultLow, forAxis: .Horizontal)
            button.addTarget(self, action: "onPressButton:", forControlEvents: .TouchUpInside)
            _buttons.append(button)
            
            if action.style == PHAlertActionStyle.Cancel {
                if hasCancelAction {
                    fatalError("Only one Cancel action is allowed")
                }
                
                hasCancelAction = true
                
                if distributeHorizontally {
                    buttons.insert(button, atIndex: 0)
                } else {
                    buttons.append(button)
                }
            } else {
                if distributeHorizontally || !hasCancelAction {
                    buttons.append(button)
                } else {
                    buttons.insert(button, atIndex: buttons.count - 1)
                }
            }
        }
        
        if buttons.isEmpty {
            return nil
        }
        
        let margins = self.delegate?.alertControllerActionButtonsMargins?(self) ?? self.alertControllerActionButtonsMargins(self)
        let padding = self.delegate?.alertControllerActionButtonsPadding?(self) ?? self.alertControllerActionButtonsPadding(self)
        let spacing = self.delegate?.alertControllerActionButtonSpacing?(self) ?? self.alertControllerActionButtonSpacing(self)
        
        let stack = UIStackView(arrangedSubviews: buttons)
//        stack.layoutMargins = padding
//        stack.layoutMarginsRelativeArrangement = true
        stack.alignment = .Fill
        
        if distributeHorizontally {
            stack.axis = .Horizontal
            stack.distribution = .FillEqually
            stack.spacing = spacing.width
        } else {
            stack.axis = .Vertical
            stack.distribution = .EqualSpacing
            stack.spacing = spacing.height
        }
        
        let actionButtonsView = self.delegate?.alertControllerActionButtonsContainerView?(self) ?? self.alertControllerActionButtonsContainerView(self)
        
        self.setupConstraintsInContainer(actionButtonsView, subview: stack, subviewMargins: padding)
        
        let containerView = UIView()
        self.setupConstraintsInContainer(containerView, subview: actionButtonsView, subviewMargins: margins)
        
        
        return containerView
    }
    
//    internal func createButtonWithTitle(title:String?, actionStyle:PHAlertActionStyle, controllerStyle:PHAlertControllerStyle) -> UIButton {
//        
//        
//        let btn = UIButton()
//        btn.backgroundColor = UIColor.yellowColor().colorWithAlphaComponent(0.75)
//        
//        guard let buttonTitle = title else {
//            btn.enabled = false
//            return btn
//        }
//        
//        var attr = [String:AnyObject]()
//        let fontSize:CGFloat = 16.0
//        
//        switch actionStyle {
//            
//            case .Default:
//                attr[NSFontAttributeName] = UIFont.systemFontOfSize(fontSize)
//                attr[NSForegroundColorAttributeName] = UIColor.blackColor()
//            
//            case .Cancel:
//                attr[NSFontAttributeName] = UIFont.boldSystemFontOfSize(fontSize)
//                attr[NSForegroundColorAttributeName] = UIColor.blackColor()
//            
//            case .Destructive:
//                attr[NSFontAttributeName] = UIFont.systemFontOfSize(fontSize)
//                attr[NSForegroundColorAttributeName] = UIColor.redColor()
//        }
//        
//        let aStr = NSAttributedString(string: buttonTitle, attributes: attr)
//        
//        btn.setAttributedTitle(aStr, forState: .Normal)
//        
//        return btn
//    }
    
    
    public func alertControllerActionButtonsContainerView(alertController: PHAlertController) -> UIView {
        
        let view = UIView()
//        view.backgroundColor = UIColor.greenColor().colorWithAlphaComponent(0.5)
        
//        let blurStyle = UIBlurEffectStyle.Light
//        let effect = UIBlurEffect(style: blurStyle)
//        let backView = UIVisualEffectView(effect: effect)
//        
//        self.setupConstraintsInContainer(view, subview: backView)
        
        
        return view
        
    }
    
    public func alertControllerActionButtonsMargins(alertController:PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
    }
    public func alertControllerActionButtonsPadding(alertController:PHAlertController) -> UIEdgeInsets {
        let p:CGFloat = 0.0
        return UIEdgeInsets(top: p, left: p, bottom: p, right: p)
    }
    public func alertControllerActionButtonSpacing(alertController:PHAlertController) -> CGSize {
        let s:CGFloat = 1.0
        return CGSize(width: s, height: s)
    }
    
    public func alertController(alertController: PHAlertController, buttonForAction action: PHAlertAction, isPreferredAction:Bool, preferredActionExists:Bool) -> UIButton {
        
        let btn:PHAlertControllerActionButton
        switch alertController.preferredStyle {
            case .Alert:
                btn = PHAlertControllerAlertActionButton()
            case .ActionSheet:
                btn = PHAlertControllerActionSheetActionButton()
        }
        btn.setAction(action, isPreferredAction: isPreferredAction, preferredActionExists: preferredActionExists)
        
//        btn.backgroundColor = UIColor.yellowColor().colorWithAlphaComponent(0.75)
        
//        guard let buttonTitle = action.title else {
//            btn.enabled = false
//        }
        
        return btn
    }
}
