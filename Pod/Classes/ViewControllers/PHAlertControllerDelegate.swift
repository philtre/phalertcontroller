//
//  PHAlertControllerDelegate.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

@objc public protocol PHAlertControllerDelegate : class, NSObjectProtocol {
    
//MARK: - Container -
    optional func alertControllerContainerView(alertController:PHAlertController) -> UIView
    optional func alertControllerContainerCornerRadius(alertController:PHAlertController) -> CGFloat
    optional func alertControllerContainerBackgroundView(alertController:PHAlertController) -> UIView?
    optional func alertController(alertController:PHAlertController, frameForContainerSize containerSize:CGSize) -> CGRect
    optional func alertController(alertController:PHAlertController, widthForContainerSize containerSize:CGSize) -> CGFloat
    optional func alertController(alertController:PHAlertController, frameOriginForAlertWithSize alertSize:CGSize, inContainerWithSize containerSize:CGSize) -> CGPoint
    
    optional func alertControllerSectionSpacing(alertController:PHAlertController) -> CGFloat
    
    optional func alertControllerDimmingView(alertController:PHAlertController) -> UIView
    
//MARK: - Header -
    
//MARK: Header Views
    
    optional func alertControllerHeaderContainerView(alertController:PHAlertController) -> UIView
    
    optional func alertController(alertController:PHAlertController, labelForTitle title:String) -> UILabel?
    optional func alertController(alertController:PHAlertController, labelForMessage message:String) -> UILabel?
    
//MARK: Header Metrics
    optional func alertControllerHeaderMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerHeaderPadding(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerHeaderSpacing(alertController:PHAlertController) -> CGFloat
    
    
//MARK: - Action Buttons -
    
//MARK: Action Button Views
    
    
    optional func alertControllerActionButtonsContainerView(alertController:PHAlertController) -> UIView
    
    optional func alertController(alertController:PHAlertController, buttonForAction action:PHAlertAction, isPreferredAction:Bool, preferredActionExists:Bool) -> UIButton
    
//MARK: Action Button Metrics
    
    optional func alertControllerActionButtonsMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerActionButtonsPadding(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerActionButtonSpacing(alertController:PHAlertController) -> CGSize
    
    
//MARK: - Numpad -
    
    optional func alertControllerNumpadDirection(alertController:PHAlertController) -> PHAlertControllerNumpadDirection
    optional func alertControllerNumpadAllowsDecimalValues(alertController:PHAlertController) -> Bool
    optional func alertControllerNumpadAllowsNegativeValues(alertController:PHAlertController) -> Bool
    optional func alertControllerNumpadDisplayFormatter(alertController:PHAlertController) -> NSNumberFormatter?
    optional func alertControllerNumpadDisplayPrefix(alertController:PHAlertController) -> String
    optional func alertControllerNumpadDisplaySuffix(alertController:PHAlertController) -> String
    
//MARK: Numpad Views
    
    optional func alertControllerNumpadContainerView(alertController:PHAlertController) -> UIView
    
//MARK: Numpad Metrics
    
    optional func alertControllerNumpadMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadPadding(alertController:PHAlertController) -> UIEdgeInsets
    
//MARK: - Numpad Display -
    
//MARK: Numpad Display Views
    
    optional func alertControllerNumpadDisplayContainerView(alertController:PHAlertController) -> UIView
    optional func alertControllerNumpadDisplayLabelBackgroundView(alertController:PHAlertController) -> UIView
    optional func alertControllerNumpadDisplayLabel(alertController:PHAlertController) -> UILabel
    optional func alertControllerNumpadClearAllButton(alertController:PHAlertController) -> UIButton
    optional func alertControllerNumpadRemoveLastDigitButton(alertController:PHAlertController) -> UIButton
    
    
//MARK: Numpad Display Metrics
    
    optional func alertControllerNumpadDisplayMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadDisplayPadding(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadDisplaySpacing(alertController:PHAlertController) -> CGFloat
    
    optional func alertControllerNumpadDisplayMinimumHeight(alertController:PHAlertController) -> CGFloat
    optional func alertControllerNumpadClearAllButtonWidth(alertController:PHAlertController) -> CGFloat
    optional func alertControllerNumpadRemoveLastDigitButtonWidth(alertController:PHAlertController) -> CGFloat
    
    optional func alertControllerNumpadDisplayLabelMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadDisplayLabelPadding(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadClearAllButtonMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadRemoveLastDigitButtonMargins(alertController:PHAlertController) -> UIEdgeInsets
    
//MARK: - Numpad Digits -
    
//MARK: Numpad Digit Views
    
    optional func alertControllerNumpadDigitsContainerView(alertController:PHAlertController) -> UIView
    
//MARK: Numpad Digit Metrics
    
    optional func alertControllerNumpadDigitsMargins(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadDigitsPadding(alertController:PHAlertController) -> UIEdgeInsets
    optional func alertControllerNumpadDigitSpacing(alertController:PHAlertController) -> CGSize
    
//MARK: Numpad Digit Buttons
    
    optional func alertController(alertController:PHAlertController, numpadButtonForDigit digit:Int) -> UIButton
    optional func alertController(alertsController:PHAlertController, numpadButtonForDecimalPointWithSymbol decimalPointSymbol:String) -> UIButton
    optional func alertControllerNumpadBottomLeftButton(alertController:PHAlertController) -> UIButton
    optional func alertControllerNumpadBottomRightButton(alertController:PHAlertController) -> UIButton
    optional func alertControllerNumpadToggleNegativeButton(alertController:PHAlertController) -> UIButton
    
    
}
