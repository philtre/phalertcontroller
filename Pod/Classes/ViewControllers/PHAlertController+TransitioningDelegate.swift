//
//  PHAlertController+TransitioningDelegate.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController : UIViewControllerTransitioningDelegate {
    
    public func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController? {
        
        switch self.preferredStyle {
            case .Alert:
                return PHAlertPresentationController(presentedViewController: presented, presentingViewController: presenting)
            case .ActionSheet:
                return PHAlertPresentationController(presentedViewController: presented, presentingViewController: presenting)
        }
    }
    
    public func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch self.preferredStyle {
            case .Alert:
                return PHAlertControllerAlertPresentationAnimator(presentedController: presented, presentingController: presenting, sourceController: source)
            case .ActionSheet:
                return PHAlertControllerActionSheetPresentationAnimator(presentedController: presented, presentingController: presenting, sourceController: source)
        }
    }
    
    public func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch self.preferredStyle {
            case .Alert:
                return PHAlertControllerAlertDismissalAnimator(dismissedController: dismissed)
            case .ActionSheet:
                return PHAlertControllerActionSheetDismissalAnimator(dismissedController: dismissed)
        }
    }
    
}

