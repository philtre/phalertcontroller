//
//  PHAlertController+DialogView.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    internal func createDialogViewForStyle(style:PHAlertControllerStyle) -> UIView {
        
        if self.title == nil && self.message == nil && _actions.count == 0 {
            fatalError("PHAlertController must have a title, message or action")
        }
        
        let containerView = self.delegate?.alertControllerContainerView?(self) ?? self.alertControllerContainerView(self)
        
        
        
        var views = [UIView]()
        
//        let stack = UIStackView()
//        stack.axis = .Vertical
//        stack.spacing = 0.0
//        stack.alignment = .Fill
//        stack.distribution = .EqualSpacing
        
//        let labelsMargins:UIEdgeInsets = self.delegate?.alertControllerMarginsForLabels?(self) ?? self.alertControllerMarginsForLabels(self)
//        let labelSpacing:CGFloat = self.delegate?.alertControllerSpacingForLabels?(self) ?? self.alertControllerSpacingForLabels(self)
        
//        let buttonsMargins:UIEdgeInsets = self.delegate?.alertControllerMarginsForButtons?(self) ?? self.alertControllerMarginsForButtons(self)
//        let buttonSpacing:CGSize = self.delegate?.alertControllerSpacingForButtons?(self) ?? self.alertControllerSpacingForButtons(self)
        
        
//        let labelsMargins = self.labelsMarginsForStyle(style)
//        let labelSpacing = self.labelSpacingForStyle(style)
//        let buttonsMargins = self.buttonsMarginsForStyle(style)
//        let buttonSpacing = self.buttonSpacingForStyle(style)
        
        if let view = self.createHeaderViewForStyle(style) {
//            view.translatesAutoresizingMaskIntoConstraints = false
            views.append(view)
        }
        
//        if let view = self.createAdditionalViewForStyle(style) {
//            view.translatesAutoresizingMaskIntoConstraints = false
//            stack.addArrangedSubview(view)
//        }
        
        if let view = self.createNumpadViewForStyle(style) {
//            view.translatesAutoresizingMaskIntoConstraints = false
            views.append(view)
        }
        
        if let view = self.createButtonsViewForStyle(style) {
//            view.translatesAutoresizingMaskIntoConstraints = false
            views.append(view)
        }
        
        let margins = [UIEdgeInsets](count:views.count, repeatedValue:UIEdgeInsets())
        let padding = UIEdgeInsets()
        let spacing = self.delegate?.alertControllerSectionSpacing?(self) ?? self.alertControllerSectionSpacing(self)
        
        self.setupVerticalConstraintsInContainer(containerView, padding: padding, subviews: views, subviewMargins: margins, spacing:spacing)
        
        
//        stack.translatesAutoresizingMaskIntoConstraints = false
//        containerView.addSubview(stack)
//
//        stack.topAnchor.constraintEqualToAnchor(containerView.topAnchor).active = true
//        stack.bottomAnchor.constraintEqualToAnchor(containerView.bottomAnchor).active = true
//        stack.leadingAnchor.constraintEqualToAnchor(containerView.leadingAnchor).active = true
//        stack.trailingAnchor.constraintEqualToAnchor(containerView.trailingAnchor).active = true
        
        
        return containerView
    }
    
    
    public func alertControllerContainerView(alertController: PHAlertController) -> UIView {
//        let cornerRadius:CGFloat = self.delegate?.alertControllerContainerCornerRadius?(self) ?? self.alertControllerContainerCornerRadius(self)
//        
//        let view = UIView()
//        view.layer.cornerRadius = cornerRadius
//        
//        
//        return view
//        return UIView()
        
        let cornerRadius:CGFloat = self.delegate?.alertControllerContainerCornerRadius?(self) ?? self.alertControllerContainerCornerRadius(self)
        
        let backgroundView:UIView? = self.delegate?.alertControllerContainerBackgroundView?(self) ?? self.alertControllerContainerBackgroundView(self)
        
//        backgroundView = UIView()
//        backgroundView?.backgroundColor = UIColor.yellowColor()
//        backgroundView = nil
        
        
        let containerView = PHAlertControllerContainerView(controllerStyle: self.preferredStyle, backgroundView: backgroundView, cornerRadius: cornerRadius)
        
        return containerView
    }
    
    public func alertControllerSectionSpacing(alertController: PHAlertController) -> CGFloat {
        return 1.0
    }
}
