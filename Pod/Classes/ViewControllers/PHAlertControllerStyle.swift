//
//  PHAlertControllerStyle.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import Foundation

public enum PHAlertControllerStyle: Int {
    case Alert
    case ActionSheet
}
