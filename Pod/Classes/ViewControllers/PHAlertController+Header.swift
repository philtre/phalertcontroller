//
//  PHAlertController+Labels.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    internal func createHeaderViewForStyle(style:PHAlertControllerStyle) -> UIView? {
//        return nil
        var title = self.title
        var message = self.message
        
        if title == nil {
            title = message
            message = nil
        }
        
        var labels = [UIView]()
        
        let titleLabel:UILabel?
        if let title = title {
            if self.delegate?.respondsToSelector("alertController:labelForTitle:") ?? false {
                titleLabel = self.delegate!.alertController!(self, labelForTitle: title)
            } else {
                titleLabel = self.alertController(self, labelForTitle: title)
            }
            
        } else {
            titleLabel = nil
        }
        if let lbl = titleLabel {
            labels.append(lbl)
        }
        
        
        let messageLabel:UILabel?
        if let message = message {
            if self.delegate?.respondsToSelector("alertController:labelForMessage:") ?? false {
                messageLabel = self.delegate!.alertController!(self, labelForMessage:message)
            } else {
                messageLabel = self.alertController(self, labelForMessage: message)
            }
            
        } else {
            messageLabel = nil
        }
        
        if let lbl = messageLabel {
            labels.append(lbl)
        }
        
        
//        if let titleLabel = self.createLabelForTitle(title, style:style) {
//            titleLabel.translatesAutoresizingMaskIntoConstraints = false
//            labels.append(titleLabel)
//        }
//        if let messageLabel = self.createLabelForMessage(message, style:style) {
//            messageLabel.translatesAutoresizingMaskIntoConstraints = false
//            labels.append(messageLabel)
//        }
        
        if labels.isEmpty {
            return nil
        }
        
//        for lbl in labels {
//            let priority:UILayoutPriority = 1
//            lbl.setContentCompressionResistancePriority(priority, forAxis: .Horizontal)
//            lbl.setContentCompressionResistancePriority(priority, forAxis: .Vertical)
//            lbl.setContentHuggingPriority(priority, forAxis: .Horizontal)
//            lbl.setContentCompressionResistancePriority(priority, forAxis: .Vertical)
//        }
        
        let margins = self.delegate?.alertControllerHeaderMargins?(self) ?? self.alertControllerHeaderMargins(self)
        let padding = self.delegate?.alertControllerHeaderPadding?(self) ?? self.alertControllerHeaderPadding(self)
        let spacing = self.delegate?.alertControllerHeaderSpacing?(self) ?? self.alertControllerHeaderSpacing(self)
        
        
        let headerView = self.delegate?.alertControllerHeaderContainerView?(self) ?? self.alertControllerHeaderContainerView(self)
        
        let stack = UIStackView(arrangedSubviews: labels)
//        stack.layoutMargins = margins
//        stack.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
//        stack.layoutMarginsRelativeArrangement = true
        stack.axis = .Vertical
        stack.spacing = spacing
        stack.alignment = .Center
        stack.distribution = .EqualSpacing
        
        self.setupConstraintsInContainer(headerView, subview: stack, subviewMargins: padding)
        
        let containerView = UIView()
        
        self.setupConstraintsInContainer(containerView, subview: headerView, subviewMargins: margins)
        
        return containerView
        
//        let outerStack = UIStackView(arrangedSubviews: [stack])
//        outerStack.layoutMargins = margins
//        outerStack.layoutMarginsRelativeArrangement = true
//        return outerStack
        
//        let stack = UIView()
//        
//        for view in labels {
//            view.translatesAutoresizingMaskIntoConstraints = false
//            stack.addSubview(view)
////            view.setContentCompressionResistancePriority(0, forAxis: .Horizontal)
////            view.centerXAnchor.constraintEqualToAnchor(stack.centerXAnchor).active = true
////            let widthConstraint = view.widthAnchor.constraintLessThanOrEqualToAnchor(stack.widthAnchor, constant: -margins.left-margins.right)
////            widthConstraint.priority = UILayoutPriorityDefaultHigh
////            widthConstraint.active = true
//            view.leadingAnchor.constraintEqualToAnchor(stack.leadingAnchor, constant: margins.left).active = true
//            view.trailingAnchor.constraintEqualToAnchor(stack.trailingAnchor, constant: -margins.right).active = true
//            
////            view.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, forAxis: .Horizontal)
////            view.setContentHuggingPriority(UILayoutPriorityDefaultLow, forAxis: .Horizontal)
////            view.widthAnchor.constraintEqualToAnchor(stack.widthAnchor).active = true
//        }
//        
//        labels.first!.topAnchor.constraintEqualToAnchor(stack.topAnchor, constant: margins.top).active = true
//        labels.last!.bottomAnchor.constraintEqualToAnchor(stack.bottomAnchor, constant: -margins.bottom).active = true
//        
//        for i in 1..<labels.count {
//            labels[i].topAnchor.constraintEqualToAnchor(labels[i-1].bottomAnchor, constant: spacing).active = true
//        }
        
        
//        return stack
    }
    
    public func alertControllerHeaderContainerView(alertController: PHAlertController) -> UIView {
        let view = UIView()
//        view.backgroundColor = UIColor.greenColor()
        return view
    }
    
    public func alertControllerHeaderMargins(alertController:PHAlertController) -> UIEdgeInsets {
        let m:CGFloat = 0.0
        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
        
    }
    public func alertControllerHeaderPadding(alertController:PHAlertController) -> UIEdgeInsets {
        
        let v:CGFloat, h:CGFloat
        
        switch alertController.preferredStyle {
            case .Alert:
                v = 22.0; h = 16.0
            case .ActionSheet:
                v = 16.0; h = 8.0
        }
        return UIEdgeInsets(top: v, left: h, bottom: v, right: h)
        
    }
    public func alertControllerHeaderSpacing(alertController:PHAlertController) -> CGFloat {
        
        switch alertController.preferredStyle {
            case .Alert:
                return 14.0
            case .ActionSheet:
                return 8.0
        }
    }
    

    public func alertController(alertController:PHAlertController, labelForTitle title:String) -> UILabel? {
        
        return self.createHeaderLabelWithTitle(title, relativeSize: 0.0, bold: true)
        
    }
    public func alertController(alertController:PHAlertController, labelForMessage message:String) -> UILabel? {
        return self.createHeaderLabelWithTitle(message, relativeSize: 0.0, bold: false)
    }
    
    private func createHeaderLabelWithTitle(title:String?, relativeSize:CGFloat, bold:Bool) -> UILabel {
        
        let baseSize:CGFloat
        let color:UIColor
        
        switch self.preferredStyle {
        case .Alert:
            baseSize = UIFont.labelFontSize()
            color = UIColor.blackColor()
        case .ActionSheet:
            baseSize = UIFont.smallSystemFontSize()
            color = UIColor.grayColor()
        }
        
        let size:CGFloat = baseSize + relativeSize
        let font:UIFont = bold ? UIFont.boldSystemFontOfSize(size) : UIFont.systemFontOfSize(size)
        
        
        let lbl = UILabel()
        lbl.textColor = color
        lbl.backgroundColor = UIColor.clearColor()
//        lbl.backgroundColor = UIColor.orangeColor().colorWithAlphaComponent(0.75)
        lbl.font = font
        lbl.textAlignment = .Center
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .ByWordWrapping
        lbl.text = title
        return lbl
    }
    
    
    
}
