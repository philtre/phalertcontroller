//
//  PHAlertController+Delegate.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-14.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

extension PHAlertController {
    
    
//MARK: Container
    
    public func alertControllerContainerCornerRadius(alertController:PHAlertController) -> CGFloat {
        return 8.0
    }
    public func alertControllerContainerBackgroundView(alertController:PHAlertController) -> UIView? {
//        return UIView()
        let blurStyle = UIBlurEffectStyle.ExtraLight
        let effect = UIBlurEffect(style: blurStyle)
        let effectView = UIVisualEffectView(effect: effect)
        
        return effectView
        
    }
    
    public func alertControllerDimmingView(alertController: PHAlertController) -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        return view
    }
    
    
}

