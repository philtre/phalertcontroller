//
//  PHAlertController.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-10-26.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit




public class PHAlertController : UIViewController, PHAlertControllerDelegate {
    
//    public var title:String?
    public var message:String?
    public var preferredStyle = PHAlertControllerStyle.Alert
    
    public var presentationEasing:PHAlertControllerEasing = .Default
    public var dismissalEasing:PHAlertControllerEasing = .Default
    
    internal var _actions = [PHAlertAction]()
    public var actions:[PHAlertAction] { return _actions }
    
    internal var _buttons = [UIButton]()
    
    internal var _numpad:PHAlertControllerNumpad?
    
    private var dialogView:UIView!
    
    public var preferredAction:PHAlertAction?
    
    public weak var delegate:PHAlertControllerDelegate?
    
    internal var dialogViewConstraints:[NSLayoutConstraint]!
    internal var dialogViewWidthConstraint:NSLayoutConstraint!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        fatalError("Initializer not implemented")
    }
    required public init?(coder aDecoder: NSCoder) {
        fatalError("Initializer not implemented")
    }
    
    public init(title:String?, message:String?, preferredStyle:PHAlertControllerStyle = PHAlertControllerStyle.Alert) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.message = message
        self.preferredStyle = preferredStyle
        self.setup()
    }
    public override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        print("viewWillTransitionToSize: \(size)")
    }
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        print("viewWillLayoutSubviews: \(self.view.bounds)")
    }
    
    
    
    private func setup() {
        self.modalPresentationStyle = .Custom
        self.transitioningDelegate = self
    }
    public func addAction(action:PHAlertAction) {
        _actions.append(action)
    }
    public func addNumpad(numberFormatter numberFormatter:NSNumberFormatter? = nil, numpadDirection:PHAlertControllerNumpadDirection? = nil, allowDecimal:Bool = true, allowNegative:Bool = true, displayPrefix:String = "", displaySuffix:String = "") {
        
        assert(_numpad == nil, "Numpad already added to PHAlertController")
        
        let formatter:NSNumberFormatter?
        if self.delegate?.respondsToSelector("alertControllerNumpadDisplayFormatter:") ?? false {
            formatter = self.delegate!.alertControllerNumpadDisplayFormatter!(self)
        } else {
            formatter = numberFormatter ?? self.alertControllerNumpadDisplayFormatter(self)
        }
        let numpad = PHAlertControllerNumpad(numberFormatter: formatter)
        
        if self.delegate?.respondsToSelector("alertControllerNumpadDirection:") ?? false {
            numpad.direction = self.delegate!.alertControllerNumpadDirection!(self)
        } else {
            numpad.direction = numpadDirection ?? self.alertControllerNumpadDirection(self)
        }
        
        if self.delegate?.respondsToSelector("alertControllerNumpadAllowsDecimalValues:") ?? false {
            numpad.allowDecimalValues = self.delegate!.alertControllerNumpadAllowsDecimalValues!(self)
        } else {
            numpad.allowDecimalValues = allowDecimal ?? self.alertControllerNumpadAllowsDecimalValues(self)
        }
        
        if self.delegate?.respondsToSelector("alertControllerNumpadAllowsNegativeValues:") ?? false {
            numpad.allowNegativeValues = self.delegate!.alertControllerNumpadAllowsNegativeValues!(self)
        } else {
            numpad.allowNegativeValues = allowNegative ?? self.alertControllerNumpadAllowsNegativeValues(self)
        }
        
        if self.delegate?.respondsToSelector("alertControllerNumpadDisplayPrefix:") ?? false {
            numpad.displayPrefix = self.delegate!.alertControllerNumpadDisplayPrefix!(self)
        } else {
            numpad.displayPrefix = displayPrefix ?? self.alertControllerNumpadDisplayPrefix(self)
        }
        
        if self.delegate?.respondsToSelector("alertControllerNumpadDisplaySuffix:") ?? false {
            numpad.displaySuffix = self.delegate!.alertControllerNumpadDisplaySuffix!(self)
        } else {
            numpad.displaySuffix = displaySuffix ?? self.alertControllerNumpadDisplaySuffix(self)
        }
        
        
        _numpad = numpad
        
        
    }
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
//        _ = UIAlertController(title: "test", message: "test", preferredStyle: .Alert)
        
//        self.view.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.5)
        
//        self.modalPresentationStyle = .Custom
//        
//        self.transitioningDelegate = self
        
        let style = self.preferredStyle
//        self.configureViewForStyle(style)
        
        let dialogView:UIView = self.createDialogViewForStyle(style)
//        let dialogWidth:CGFloat
        
        
        
        dialogView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(dialogView)
        
        let topConstraint = dialogView.topAnchor.constraintEqualToAnchor(self.view.topAnchor)
        let leadingConstraint = dialogView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
        let trailingConstraint = dialogView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
//        let bottomContstraint = dialogView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor)
//        bottomContstraint.priority = 2
        
        self.dialogViewConstraints = [
            topConstraint,
            leadingConstraint,
            trailingConstraint,
//            bottomContstraint,
        ]
        self.dialogViewWidthConstraint = dialogView.widthAnchor.constraintEqualToConstant(0)
        
        for constraint in self.dialogViewConstraints {
//            constraint.priority = UILayoutPriorityDefaultHigh
            constraint.priority--
            constraint.active = true
        }
    
        
        self.dialogView = dialogView
//        dialogView.widthAnchor.constraintEqualToConstant(270.0)
        
//        self.view.layer.cornerRadius = 6.0
//        self.view.layer.masksToBounds = true
        
//        dialogView.layoutIfNeeded()
        
//        let size = dialogView.bounds.size
        
//        self.preferredContentSize = size
        
//        let size = self.view.subviews.first!.sizeThatFits(CGSize(width: 300, height: CGFloat.max))
        
//        let size = CGSize(width: 300, height: 600)
        
//        self.preferredContentSize = size
        
        
    }
    
    public func frameForContainerSize(containerSize:CGSize) -> CGRect {
        
        
        let fr = self.delegate?.alertController?(self, frameForContainerSize: containerSize) ?? self.alertController(self, frameForContainerSize: containerSize)
        
        return fr
    }
    
    public func alertController(alertController: PHAlertController, frameForContainerSize containerSize: CGSize) -> CGRect {
        
        
//        return CGRect(x:52.0, y:209.0, width:ceil(containerSize.width*0.72), height:300)
//        CATransaction.begin()
//        CATransaction.setDisableActions(true)
        
        for constraint in self.dialogViewConstraints {
            constraint.active = false
        }
        
//        let originalFrame = self.view.frame
        
        
        
//        print("originalFrame: \(originalFrame)")
        
//        var fr = originalFrame
        var fr = CGRect.zero
        
        let width:CGFloat = self.delegate?.alertController?(self, widthForContainerSize: containerSize) ?? self.alertController(self, widthForContainerSize: containerSize)
        
        self.dialogViewWidthConstraint.constant = width
        self.dialogViewWidthConstraint.active = true
        self.dialogView.layoutIfNeeded()
        fr.size = self.dialogView.bounds.size
        fr.origin = self.delegate?.alertController?(self, frameOriginForAlertWithSize: fr.size, inContainerWithSize: containerSize) ?? self.alertController(self, frameOriginForAlertWithSize: fr.size, inContainerWithSize: containerSize)
        
        self.dialogViewWidthConstraint.active = false
        
//        self.view.frame = fr
        for constraint in self.dialogViewConstraints {
            constraint.active = true
        }
//        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
        print("frameForContainerSize: \(fr)")
        
//        self.view.frame = originalFrame
        
//        self.view.layoutIfNeeded()
        
//        CATransaction.commit()
        
        return fr
        
    }
    

    public func alertController(alertController: PHAlertController, widthForContainerSize containerSize: CGSize) -> CGFloat {
        
        switch self.preferredStyle {
            case .Alert:
                return 270.0
            case .ActionSheet:
                return min(containerSize.width - 40.0, 340.0)
        }
    }
    
    public func alertController(alertController: PHAlertController, frameOriginForAlertWithSize alertSize: CGSize, inContainerWithSize containerSize: CGSize) -> CGPoint {
        
        let x:CGFloat, y:CGFloat
        
        switch self.preferredStyle {
            case .Alert:
                x = floor((containerSize.width - alertSize.width)/2)
                y = floor((containerSize.height - alertSize.height)/2)
            case .ActionSheet:
                x = floor((containerSize.width - alertSize.width)/2)
                y = containerSize.height - alertSize.height
        }
        return CGPoint(x: x, y: y)
        
    }
    
//    public func configureViewForStyle(style:PHAlertControllerStyle) {
//        self.view.backgroundColor = self.backgroundColorForStyle(style)
////        self.view.layer.cornerRadius = 5
////        self.view.layer.masksToBounds = true
//    }
    
//    public func backgroundColorForStyle(style:PHAlertControllerStyle) -> UIColor {
//        return UIColor.clearColor()
////        return UIColor.cyanColor()
//    }
    
    
    
    
//    private func createAdditionalViewForStyle(style:PHAlertControllerStyle) -> UIView? {
//        return nil
//    }
    
    
    
//    public func labelsMarginsForStyle(style:PHAlertControllerStyle) -> UIEdgeInsets {
//        let m:CGFloat = 16.0
//        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
//    }
//    public func buttonsMarginsForStyle(style:PHAlertControllerStyle) -> UIEdgeInsets {
//        let m:CGFloat = 0.0
//        return UIEdgeInsets(top: m, left: m, bottom: m, right: m)
//    }
//    public func buttonSpacingForStyle(style:PHAlertControllerStyle) -> CGSize {
//        let s:CGFloat = 1.0
//        return CGSize(width: s, height: s)
//    }
//    public func labelSpacingForStyle(style:PHAlertControllerStyle) -> CGFloat {
//        let s:CGFloat = 8.0
//        return s
//    }
    
    
    
    @objc private func onPressButton(button:UIButton) {
        
        let action = self.actionForButton(button)
        
        let completion = {
            if let handler = action.handler {
                handler(action)
            }
        }
        
        if !action.shouldDismiss {
            completion()
            return
        }
        
        self.dismissViewControllerAnimated(true, completion: completion)
        
    }
    
    private func actionForButton(button:UIButton) -> PHAlertAction {
        let index = _buttons.indexOf(button)!
        return _actions[index]
    }
    
//    public func createLabelForTitle(title:String?, style:PHAlertControllerStyle) -> UILabel? {
//        guard let title = title else {
//            return nil
//        }
//        let lbl = UILabel()
//        lbl.textColor = UIColor.blackColor()
//        lbl.backgroundColor = UIColor.clearColor()
//        lbl.backgroundColor = UIColor.orangeColor().colorWithAlphaComponent(0.75)
//        lbl.font = UIFont.boldSystemFontOfSize(18.0)
//        lbl.textAlignment = .Center
//        lbl.numberOfLines = 0
//        lbl.lineBreakMode = .ByWordWrapping
//        lbl.text = title
//        return lbl
//    }
    
//    public func createLabelForMessage(message:String?, style:PHAlertControllerStyle) -> UILabel? {
//        guard let message = message else {
//            return nil
//        }
//        let lbl = UILabel()
//        lbl.textColor = UIColor.blackColor()
//        lbl.backgroundColor = UIColor.clearColor()
//        lbl.backgroundColor = UIColor.orangeColor().colorWithAlphaComponent(0.75)
//        lbl.font = UIFont.systemFontOfSize(16.0)
//        lbl.textAlignment = .Center
//        lbl.numberOfLines = 0
//        lbl.lineBreakMode = .ByWordWrapping
//        lbl.text = message
//        return lbl
//    }
    
//    public func createButtonForAction(action:PHAlertAction, style:PHAlertControllerStyle) -> UIButton {
//        
//        return self.createButtonWithTitle(action.title ?? "", actionStyle: action.style, controllerStyle:style)
//    }
    
    
}

