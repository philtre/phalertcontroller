//
//  PHAlertActionStyle.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-11.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import Foundation

public enum PHAlertActionStyle {
    case Default
    case Cancel
    case Destructive
}

