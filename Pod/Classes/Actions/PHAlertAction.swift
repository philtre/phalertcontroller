//
//  PHAlertAction.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-11.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import Foundation

@objc public class PHAlertAction : NSObject {
    
    public private(set) var title: String?
    public private(set) var style: PHAlertActionStyle
    public var enabled: Bool = true
    public var shouldDismiss: Bool = true
    
    internal var handler:((PHAlertAction) -> Void)?
    
    public init(title: String?, style: PHAlertActionStyle, shouldDismiss:Bool = true, handler: ((PHAlertAction) -> Void)?) {
        self.title = title
        self.style = style
        self.shouldDismiss = shouldDismiss
        self.handler = handler
    }
    
    
}
