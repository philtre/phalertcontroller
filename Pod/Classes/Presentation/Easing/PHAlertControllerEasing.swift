//
//  PHAlertControllerEasing.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-10.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import Foundation
import CoreGraphics
import QuartzCore

public enum PHAlertControllerEasing {
    
    case Default
    
    case Linear
    
    case SoftIn
    case SoftOut
    
    case SofterIn
    case SofterOut
    
    case SoftInOut
    case SofterInOut
    
    case CustomInOut(curve:Float)
    case CustomInCustomOut(inCurve:Float, outCurve:Float)
    
    case ExponentialIn
    case ExponentialOut
    
    case SmallOvershootIn
    case SmallOvershootOut
    
    case BigOvershootIn
    case BigOvershootOut
    
    case CustomOvershootIn(overshoot:Float)
    case CustomOvershootOut(overshoot:Float)
    
    case Custom(c1x:Float, c1y:Float, c2x:Float, c2y:Float)
    
    
    var timingFunction:CAMediaTimingFunction {
        
        switch self {
            
        case .Default:
            return CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
            
        case .Linear:
            return CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            
        case .SoftIn:
            return CAMediaTimingFunction(controlPoints: 0.6, 0.0, 0.8, 1.0)
        case .SoftOut:
            return CAMediaTimingFunction(controlPoints: 0.2, 0.0, 0.4, 1.0)
            
        case .SofterIn:
            return CAMediaTimingFunction(controlPoints: 0.7, 0.0, 0.7, 1.0)
        case .SofterOut:
            return CAMediaTimingFunction(controlPoints: 0.3, 0.0, 0.3, 1.0)
            
        case .SoftInOut:
            return CAMediaTimingFunction(controlPoints: 0.5, 0.0, 0.5, 1.0)
        case .SofterInOut:
            return CAMediaTimingFunction(controlPoints: 0.75, 0.0, 0.25, 1.0)
            
        case .CustomInOut(let curve):
            return CAMediaTimingFunction(controlPoints: curve, 0.0, 1-curve, 1.0)
        case .CustomInCustomOut(let inCurve, let outCurve):
            return CAMediaTimingFunction(controlPoints: inCurve, 0.0, 1-outCurve, 1.0)
            
        case .ExponentialIn:
            return CAMediaTimingFunction(controlPoints: 0.67, 0.0, 0.86, 0.0)
        case .ExponentialOut:
            return CAMediaTimingFunction(controlPoints: 0.14, 1.00, 0.33, 1.0)
            
        case .SmallOvershootIn:
            return CAMediaTimingFunction(controlPoints: 0.33, 0.0, 0.67, -0.4)
        case .SmallOvershootOut:
            return CAMediaTimingFunction(controlPoints: 0.33, 1.4, 0.33, 1.0)
            
        case .BigOvershootIn:
            return CAMediaTimingFunction(controlPoints: 0.33, 0.0, 0.67, -0.57)
        case .BigOvershootOut:
            return CAMediaTimingFunction(controlPoints: 0.33, 1.57, 0.67, 1.0)
            
        case .CustomOvershootIn(let overshoot):
            return CAMediaTimingFunction(controlPoints: 0.33, 0.0, 0.67, -overshoot)
        case .CustomOvershootOut(let overshoot):
            return CAMediaTimingFunction(controlPoints: 0.33, 1.0 + overshoot, 0.67, 1.0)
            
        case .Custom(let c1x, let c1y, let c2x, let c2y):
            return CAMediaTimingFunction(controlPoints: c1x, c1y, c2x, c2y)
        }
        
        
    }
    
}
