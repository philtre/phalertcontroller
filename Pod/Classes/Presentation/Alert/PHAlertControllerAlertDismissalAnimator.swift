//
//  PHAlertControllerAlertDismissalAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-11-13.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerAlertDismissalAnimator : PHAlertControllerDismissalAnimator {
    
//    override func transitionTimingFunction(transitionContext: UIViewControllerContextTransitioning?) -> CAMediaTimingFunction {
//        return CAMediaTimingFunction(controlPoints: 0.67, 0.0, 0.86, 0.0)
//    }
    override func defaultTransitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        return .ExponentialIn
    }
    override func finalTransform(transitionContext: UIViewControllerContextTransitioning?) -> CATransform3D {
        let s:CGFloat = 0.5
        return CATransform3DMakeScale(s, s, 1.0)
    }
    override func finalOpacity(transitionContext: UIViewControllerContextTransitioning?) -> Float {
        return 0.0
    }
    
    
}
//@objc class PHAlertControllerAlertDismissalAnimator : NSObject, UIViewControllerAnimatedTransitioning {
//    
//    weak var dismissedController:UIViewController?
//    
//    init(dismissedController:UIViewController) {
//        self.dismissedController = dismissedController
//    }
//    
//    
//    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
//        return 4.0
//    }
//    
//    
//    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
//        let originView:UIView? = transitionContext.viewForKey(UITransitionContextFromViewKey)
//        let alertView = originView!
//        
//        let dur = self.transitionDuration(transitionContext)
//        
//        let initialTransform = CATransform3DIdentity
//        let finalTransform = CATransform3DMakeScale(0.0, 0.0, 1.0)
//        let initialVal = NSValue(CATransform3D: initialTransform)
//        let finalVal = NSValue(CATransform3D: finalTransform)
//        
//        let anim = CABasicAnimation(keyPath: "transform")
//        anim.timingFunction = CAMediaTimingFunction(controlPoints: 0.67, 0.0, 0.86, 0.0)
//        anim.duration = dur
//        anim.fromValue = initialVal
//        anim.toValue = finalVal
//        
//        let animKey = "DismissAlert"
//        alertView.layer.transform = finalTransform
//        CATransaction.begin()
////        CATransaction.setCompletionBlock { () -> Void in
////            transitionContext.completeTransition(true)
////        }
//        alertView.layer.addAnimation(anim, forKey: animKey)
//        CATransaction.commit()
//        UIView.animateWithDuration(dur, animations: {}, completion: {finished in transitionContext.completeTransition(finished)})
//    }
//    
//    func animationEnded(transitionCompleted: Bool) {
//        print("animationEnded")
//    }
//}
