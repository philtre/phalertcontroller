//
//  PHAlertControllerAlertPresentationAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-11-13.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerAlertPresentationAnimator : PHAlertControllerPresentationAnimator {
//    override func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
//        return 4.0
//    }
//    override func transitionTimingFunction(transitionContext: UIViewControllerContextTransitioning?) -> CAMediaTimingFunction {
//        
//        return CAMediaTimingFunction(controlPoints: 0.33, 1.40, 0.33, 1.0)
//    }
    override func defaultTransitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        return .ExponentialOut
//        return .SmallOvershootOut
    }
    override func initialTransform(transitionContext: UIViewControllerContextTransitioning?) -> CATransform3D {
        let s:CGFloat = 1.5
        return CATransform3DMakeScale(s, s, 1.0)
//        return CATransform3DMakeScale(0.1, 0.1, 1.0)
    }
    override func initialOpacity(transitionContext: UIViewControllerContextTransitioning?) -> Float {
        return 0.0
    }
    
}
//@objc class PHAlertControllerAlertPresentationAnimator : NSObject, UIViewControllerAnimatedTransitioning {
//    
//    weak var presentedController:UIViewController?
//    weak var presentingController:UIViewController?
//    weak var sourceController:UIViewController?
//    
//    init(presentedController:UIViewController, presentingController:UIViewController, sourceController:UIViewController) {
//        self.presentedController = presentedController
//        self.presentingController = presentingController
//        self.sourceController = sourceController
//    }
//    
//    
//    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
//        return 4.0
//    }
//    
//    
//    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
//        print("animateTransition")
//        
//        let targetView:UIView? = transitionContext.viewForKey(UITransitionContextToViewKey)
//        let targetVC:UIViewController? = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
//        let targetVCFinalFrame:CGRect? = transitionContext.finalFrameForViewController(targetVC!)
//        
//        let alertView = targetView!
//        alertView.frame = targetVCFinalFrame ?? .zero
//        
//        let dur = self.transitionDuration(transitionContext)
//        
//        let initialTransform = CATransform3DMakeScale(0.1, 0.1, 1.0)
//        let finalTransform = CATransform3DIdentity
//        let initialVal = NSValue(CATransform3D: initialTransform)
//        let finalVal = NSValue(CATransform3D: finalTransform)
//    
//        let anim = CABasicAnimation(keyPath: "transform")
//        anim.timingFunction = CAMediaTimingFunction(controlPoints: 0.33, 1.40, 0.33, 1.0)
//        anim.duration = dur
//        anim.fromValue = initialVal
//        anim.toValue = finalVal
//        
//        let animKey = "PresentAlert"
//        alertView.layer.transform = finalTransform
//        CATransaction.begin()
////        CATransaction.setCompletionBlock { () -> Void in
////            transitionContext.completeTransition(true)
////        }
//        alertView.layer.addAnimation(anim, forKey: animKey)
//        CATransaction.commit()
//        UIView.animateWithDuration(dur, animations: {}, completion: {finished in transitionContext.completeTransition(finished)})
//    }
//    
//    
//    
//    func animationEnded(transitionCompleted: Bool) {
//        print("animationEnded")
//    }
//}
//
