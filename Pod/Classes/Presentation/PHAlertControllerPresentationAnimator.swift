//
//  PHAlertControllerPresentationAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-09.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerPresentationAnimator : PHAlertControllerAnimator {
    
    weak var presentedController:UIViewController?
    weak var presentingController:UIViewController?
    weak var sourceController:UIViewController?
    
    init(presentedController:UIViewController, presentingController:UIViewController, sourceController:UIViewController) {
        self.presentedController = presentedController
        self.presentingController = presentingController
        self.sourceController = sourceController
    }
    
    override func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.4
    }
    
    override func transitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        if let vc = self.alertViewController(transitionContext) as? PHAlertController {
            return vc.presentationEasing
        }
        return super.transitionEasing(transitionContext)
    }
    
    override func alertViewController(transitionContext: UIViewControllerContextTransitioning?) -> UIViewController? {
        return transitionContext?.viewControllerForKey(UITransitionContextToViewControllerKey)
    }
    override func alertView(transitionContext: UIViewControllerContextTransitioning?) -> UIView? {
        return transitionContext?.viewForKey(UITransitionContextToViewKey)
    }
    
    override func finalFrame(transitionContext: UIViewControllerContextTransitioning?) -> CGRect {
        return transitionContext?.finalFrameForViewController(self.alertViewController(transitionContext)!) ?? .zero
    }
    
}

