//
//  PHAlertPresentationController.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-11-13.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertPresentationController : UIPresentationController {
    
    private var dimmingView:UIView!
    private var dimmingViewTapGesture:UITapGestureRecognizer!
    
    override func presentationTransitionWillBegin() {
        
        let dimmingView:UIView
        if let alertController = self.presentedViewController as? PHAlertController {
            dimmingView = alertController.delegate?.alertControllerDimmingView?(alertController) ?? alertController.alertControllerDimmingView(alertController)
        } else {
            dimmingView = UIView()
            dimmingView.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.5)
        }
        
        self.dimmingView = dimmingView
        
        self.dimmingViewTapGesture = UITapGestureRecognizer(target: self, action: "onTapDimmingView:")
        self.dimmingViewTapGesture.enabled = false
        self.dimmingView.addGestureRecognizer(self.dimmingViewTapGesture)
        
        
        dimmingView.frame = self.containerView?.bounds ?? .zero
        dimmingView.alpha = 0.0
        self.containerView?.insertSubview(dimmingView, atIndex: 0)
//        self.containerView?.addSubview(self.presentingViewController.view)
        self.containerView?.addSubview(self.presentedView()!)
//        let dur = self.presentedViewController.transitionCoordinator()?.transitionDuration()
        
        self.presentedViewController.transitionCoordinator()?.animateAlongsideTransition(
                    { context in
                        self.dimmingView.alpha = 1.0
                    },
                completion:
                    { context in
                        print("presentation complete")
                        self.dimmingViewTapGesture.enabled = true
                    }
        )
    }
    override func dismissalTransitionWillBegin() {
        self.dimmingViewTapGesture.enabled = false
        self.presentedViewController.transitionCoordinator()?.animateAlongsideTransition(
                    { context in
                        self.dimmingView.alpha = 0.0
                    },
                completion:
                    { context in
                        print("dismissal complete")
                    }
        )
    }
    
    override func frameOfPresentedViewInContainerView() -> CGRect {
        if let alertController = self.presentedViewController as? PHAlertController, let bounds = self.containerView?.bounds {
            let fr = alertController.frameForContainerSize(bounds.size)
            return fr
        }
//        var presentedViewFrame = CGRect.zero
//        let containerBounds = self.containerView?.bounds ?? .zero
//        
//        presentedViewFrame.size = self.sizeForChildContentContainer(self.presentedViewController, withParentContainerSize: containerBounds.size)
//        
//        presentedViewFrame.origin.x = floor((containerBounds.size.width - presentedViewFrame.size.width)/2)
//        presentedViewFrame.origin.y = floor((containerBounds.size.height - presentedViewFrame.size.height)/2)
        
//        return presentedViewFrame
        return .zero
    }
    
    override func sizeForChildContentContainer(container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        if let alertController = container as? PHAlertController {
            let fr = alertController.frameForContainerSize(parentSize)
            return fr.size
        }
        return .zero
//        return container.preferredContentSize
    }
    @objc private func onTapDimmingView(gesture:UITapGestureRecognizer) {
        self.presentedViewController.dismissViewControllerAnimated(true) { () -> Void in
            print("dismissed by tapping dimming view")
        }
    }
    
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        print("containerViewWillLayoutSubviews: \(self.containerView?.bounds)")
        self.dimmingView.frame = self.containerView?.bounds ?? .zero
//        CATransaction.begin()
//        CATransaction.setDisableActions(true)
        if let presentedView = self.presentedView() {
            let fr = self.frameOfPresentedViewInContainerView()
            presentedView.frame = fr
//            presentedView.layoutIfNeeded()
        }
//        CATransaction.commit()
    }
    
//    override func presentedView() -> UIView? {
//    }
    
}


