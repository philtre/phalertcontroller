//
//  PHAlertControllerDismissalAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-09.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerDismissalAnimator : PHAlertControllerAnimator {
    
    weak var dismissedController:UIViewController?
    
    init(dismissedController:UIViewController) {
        self.dismissedController = dismissedController
    }
    
    override func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.2
    }
    override func transitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        if let vc = self.alertViewController(transitionContext) as? PHAlertController {
            return vc.dismissalEasing
        }
        return super.transitionEasing(transitionContext)
    }
    override func alertViewController(transitionContext: UIViewControllerContextTransitioning?) -> UIViewController? {
        return transitionContext?.viewControllerForKey(UITransitionContextFromViewControllerKey)
    }
    override func alertView(transitionContext: UIViewControllerContextTransitioning?) -> UIView? {
        return transitionContext?.viewForKey(UITransitionContextFromViewKey)
    }
    
    override func finalFrame(transitionContext: UIViewControllerContextTransitioning?) -> CGRect {
        return transitionContext?.finalFrameForViewController(self.alertViewController(transitionContext)!) ?? .zero
    }
    
}


