//
//  PHAlertControllerActionSheetPresentationAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-11-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerActionSheetPresentationAnimator : PHAlertControllerPresentationAnimator {
    
//    override func transitionTimingFunction(transitionContext: UIViewControllerContextTransitioning?) -> CAMediaTimingFunction {
//        return CAMediaTimingFunction(controlPoints: 0.14, 1.0, 0.33, 1.0)
//    }
    override func defaultTransitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        return .ExponentialOut
    }
    override func initialTransform(transitionContext: UIViewControllerContextTransitioning?) -> CATransform3D {
        let finalFrame = self.finalFrame(transitionContext)
        return CATransform3DMakeTranslation(0.0, finalFrame.size.height, 0.0)
    }
}

//@objc class PHAlertControllerActionSheetPresentationAnimator : NSObject, UIViewControllerAnimatedTransitioning {
//    
//    weak var presentedController:UIViewController?
//    weak var presentingController:UIViewController?
//    weak var sourceController:UIViewController?
//    
//    init(presentedController:UIViewController, presentingController:UIViewController, sourceController:UIViewController) {
//        self.presentedController = presentedController
//        self.presentingController = presentingController
//        self.sourceController = sourceController
//    }
//    
//    
//    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
//        return 4.0
//    }
//    
//    
//    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
//        print("animateTransition")
//        
//        let targetView:UIView? = transitionContext.viewForKey(UITransitionContextToViewKey)
//        let targetVC:UIViewController? = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
//        let targetVCFinalFrame:CGRect = transitionContext.finalFrameForViewController(targetVC!) ?? .zero
//        
//        
//        let alertView = targetView!
//        alertView.frame = targetVCFinalFrame
//        
//        let dur = self.transitionDuration(transitionContext)
//        
//        let initialTransform = CATransform3DMakeTranslation(0.0, targetVCFinalFrame.size.height, 0.0)
//        let finalTransform = CATransform3DIdentity
//        
//        let initialVal = NSValue(CATransform3D:initialTransform)
//        let finalVal = NSValue(CATransform3D:finalTransform)
//        
//        
//        let anim = CABasicAnimation(keyPath: "transform")
//        anim.timingFunction = CAMediaTimingFunction(controlPoints: 0.14, 1.0, 0.33, 1.0)
//        anim.duration = dur
//        anim.fromValue = initialVal
//        anim.toValue = finalVal
//        
//        let animKey = "PresentActionSheet"
//        alertView.layer.transform = finalTransform
//        CATransaction.begin()
//        CATransaction.setCompletionBlock { () -> Void in
//            transitionContext.completeTransition(true)
//        }
//        alertView.layer.addAnimation(anim, forKey: animKey)
//        CATransaction.commit()
//        UIView.animateWithDuration(dur, animations: {}, completion: {finished in transitionContext.completeTransition(finished)})
//    }
//    
//    
//    
//    func animationEnded(transitionCompleted: Bool) {
//        print("animationEnded")
//    }
//}


