//
//  PHAlertControllerActionSheetDismissalAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-11-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerActionSheetDismissalAnimator : PHAlertControllerDismissalAnimator {
    
//    override func transitionTimingFunction(transitionContext: UIViewControllerContextTransitioning?) -> CAMediaTimingFunction {
//        return CAMediaTimingFunction(controlPoints: 0.67, 0.0, 0.86, 0.0)
//    }
    override func defaultTransitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        return .ExponentialIn
    }
    override func finalTransform(transitionContext: UIViewControllerContextTransitioning?) -> CATransform3D {
        let finalFrame = self.finalFrame(transitionContext)
        return CATransform3DMakeTranslation(0.0, finalFrame.size.height, 0.0)
    }
    
    
}


