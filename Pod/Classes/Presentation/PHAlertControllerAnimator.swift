//
//  PHAlertControllerAnimator.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-09.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

@objc class PHAlertControllerAnimator : NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 4.0
    }
    
    
    func alertViewController(transitionContext: UIViewControllerContextTransitioning?) -> UIViewController? {
        return nil
    }
    func alertView(transitionContext: UIViewControllerContextTransitioning?) -> UIView? {
        return nil
    }
    func initialTransform(transitionContext: UIViewControllerContextTransitioning?) -> CATransform3D {
        return CATransform3DIdentity
    }
    func finalTransform(transitionContext: UIViewControllerContextTransitioning?) -> CATransform3D {
        return CATransform3DIdentity
    }
    func initialOpacity(transitionContext: UIViewControllerContextTransitioning?) -> Float {
        return 1.0
    }
    func finalOpacity(transitionContext: UIViewControllerContextTransitioning?) -> Float {
        return 1.0
    }
    func finalFrame(transitionContext: UIViewControllerContextTransitioning?) -> CGRect {
        return .zero
    }
    
    func transitionTimingFunction(transitionContext: UIViewControllerContextTransitioning?) -> CAMediaTimingFunction {
        
        let easing = self.transitionEasing(transitionContext)
        
        switch easing {
            case .Default:
                return self.defaultTransitionEasing(transitionContext).timingFunction
            default:
                return easing.timingFunction
        }
    }
    
    func transitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        return .Default
    }
    
    func defaultTransitionEasing(transitionContext: UIViewControllerContextTransitioning?) -> PHAlertControllerEasing {
        return .Default
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        print("animateTransition")
        
        let dur = self.transitionDuration(transitionContext)
        
        let alertView = self.alertView(transitionContext)!
        alertView.frame = self.finalFrame(transitionContext)
        
        let initialTransform = self.initialTransform(transitionContext)
        let finalTransform = self.finalTransform(transitionContext)
        
        let initialOpacity = self.initialOpacity(transitionContext)
        let finalOpacity = self.finalOpacity(transitionContext)
        
        var anims = [CAAnimation]()
        
        if !CATransform3DEqualToTransform(initialTransform, finalTransform) {
            let initialVal = NSValue(CATransform3D:initialTransform)
            let finalVal = NSValue(CATransform3D:finalTransform)
            
            
            let anim = CABasicAnimation(keyPath: "transform")
            anim.timingFunction = self.transitionTimingFunction(transitionContext)
//            anim.duration = dur
            anim.fromValue = initialVal
            anim.toValue = finalVal
            anims.append(anim)
            
        }
        if initialOpacity != finalOpacity {
            let initialVal:NSNumber = initialOpacity
            let finalVal:NSNumber = finalOpacity
            
            
            let anim = CABasicAnimation(keyPath: "opacity")
            anim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            //            anim.timingFunction = self.transitionTimingFunction(transitionContext)
            //            anim.duration = dur
            anim.fromValue = initialVal
            anim.toValue = finalVal
            anims.append(anim)
        }
        
        alertView.layer.opacity = finalOpacity
        alertView.layer.transform = finalTransform
        
        let animKey = "Alert"
//        CATransaction.setCompletionBlock { () -> Void in
//            transitionContext.completeTransition(true)
//        }
        
        let anim:CAAnimation
        
        switch anims.count {
            case 0:
                transitionContext.completeTransition(true)
                return
            case 1:
                anim = anims.first!
            default:
                let group = CAAnimationGroup()
                group.animations = anims
                anim = group
        }
        
        anim.duration = dur
        
        CATransaction.begin()
        
        alertView.layer.addAnimation(anim, forKey: animKey)
        
        CATransaction.commit()
        UIView.animateWithDuration(
            dur,
            animations: {},
            completion: { finished in
                transitionContext.completeTransition(finished)
            }
        )
    }
    
    
    
    func animationEnded(transitionCompleted: Bool) {
        print("animationEnded")
    }
}
