//
//  PHAlertControllerActionButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerActionButton : PHAlertControllerDefaultButton {
    
    internal var ignorePreferred:Bool {
        return false
    }
    
    internal func setAction(action:PHAlertAction?, isPreferredAction:Bool = false, preferredActionExists:Bool = false) {
        
        guard let action = action, title = action.title else {
            self.setTitle(nil, font: nil, color: nil, forState: .Normal)
            return
        }
        
        let font:UIFont
        let color:UIColor
        
        if action.style == .Destructive {
            color = UIColor.redColor()
        } else {
            color = UIColor.blackColor()
        }
        
        
        let fontSize:CGFloat = UIFont.buttonFontSize()
        
        var bold:Bool = false
        
        if action.style == .Cancel {
            
            if !preferredActionExists || self.ignorePreferred || isPreferredAction {
                bold = true
            }
            
        } else if !self.ignorePreferred && isPreferredAction {
            bold = true
        }
        
        if bold {
            font = UIFont.boldSystemFontOfSize(fontSize)
        } else {
            font = UIFont.systemFontOfSize(fontSize)
        }
        
        self.setTitle(title, font: font, color: color, forState: .Normal)
        
    }
    
}
