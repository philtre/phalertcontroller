//
//  PHAlertControllerActionSheetActionButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

internal class PHAlertControllerActionSheetActionButton : PHAlertControllerActionButton {
    
    override var ignorePreferred:Bool {
        return true
    }
    override var minimumIntrinsicContentSize: CGSize {
        return CGSize(width: 0.0, height: 56.0)
    }
    
}
