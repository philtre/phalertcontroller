//
//  PHAlertControllerContainerView.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-11.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerContainerView : UIView {
    
    
    private var backgroundView:UIView? {
        willSet {
            self.backgroundView?.removeFromSuperview()
        }
        didSet {
            if let backgroundView = self.backgroundView {
                self.insertSubview(backgroundView, atIndex: 0)
            }
            self.setNeedsLayout()
        }
    }
    private var maskLayer:CAShapeLayer!
    
    private(set) var cornerRadius:CGFloat = 0 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    var controllerStyle:PHAlertControllerStyle = .Alert {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    convenience init(controllerStyle:PHAlertControllerStyle, backgroundView:UIView?, cornerRadius:CGFloat) {
        self.init(frame:.zero)
        self.controllerStyle = controllerStyle
        if let backgroundView = backgroundView {
            self.backgroundView = backgroundView
            self.addSubview(backgroundView)
        }
        if let backgroundView = backgroundView {
            self.backgroundView = backgroundView
            self.insertSubview(backgroundView, atIndex: 0)
        }
        self.cornerRadius = cornerRadius
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        
//        let blurStyle = UIBlurEffectStyle.ExtraLight
//        let effect = UIBlurEffect(style: blurStyle)
//        let backView = UIVisualEffectView(effect: effect)
//        self.addSubview(backView)
//        self.backgroundView = backView
        
        
        let maskLayer = CAShapeLayer()
//        maskLayer.fillColor = UIColor.blackColor().CGColor
        self.layer.addSublayer(maskLayer)
        self.maskLayer = maskLayer
        self.layer.mask = maskLayer
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let bounds = self.bounds
        self.backgroundView?.frame = bounds
        
        let corners:UIRectCorner
        
        switch self.controllerStyle {
            case .Alert:
                corners = UIRectCorner.AllCorners
            case .ActionSheet:
                corners = [.TopLeft, .TopRight]
        }
        let cornerRadii = CGSizeMake(self.cornerRadius, self.cornerRadius)
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: cornerRadii)
        self.maskLayer.frame = bounds
        self.maskLayer.path = path.CGPath
        
    }
}
