//
//  PHAlertControllerButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

internal class PHAlertControllerButton : UIButton {
    
    private var backgroundColorsByState:[UInt:UIColor]?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        self.updateState()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
        self.updateState()
    }
    
    internal func setup() {
        
    }
    
    
    override var highlighted:Bool {
        didSet{
            self.updateState()
        }
    }
    override var selected:Bool {
        didSet{
            self.updateState()
        }
    }
    override var enabled:Bool {
        didSet{
            self.updateState()
        }
    }
    
    internal func colorForCurrentStateFromDict(inout dict:[UInt:UIColor]?) -> UIColor? {
        if let color = dict?[self.state.rawValue] {
            return color
        }
        if !self.enabled {
            if let color = dict?[UIControlState.Disabled.rawValue] {
                return color
            }
        }
        if self.selected {
            if let color = dict?[UIControlState.Selected.rawValue] {
                return color
            }
        }
        if self.highlighted {
            if let color = dict?[UIControlState.Highlighted.rawValue] {
                return color
            }
        }
        return dict?[UIControlState.Normal.rawValue]
    }
    
    func updateState() {
        self.layer.backgroundColor = self.colorForCurrentStateFromDict(&self.backgroundColorsByState)?.CGColor ?? UIColor.clearColor().CGColor
    }
    
    func setBackgroundColor(color: UIColor?, forState state: UIControlState) {
        if color != nil && self.backgroundColorsByState == nil {
            self.backgroundColorsByState = [UInt:UIColor]()
        }
        self.backgroundColorsByState?[state.rawValue] = color
    }
    
    internal var minimumIntrinsicContentSize: CGSize {
        return .zero
    }
    
    override func intrinsicContentSize() -> CGSize {
        var size = super.intrinsicContentSize()
        let minSize = self.minimumIntrinsicContentSize
        size.width = max(size.width, minSize.width)
        size.height = max(size.height, minSize.height)
        return size
    }
    
    internal func setTitle(title: String?, font:UIFont?, color:UIColor?, forState state: UIControlState) {
        guard let title = title else {
            self.setAttributedTitle(nil, forState: state)
            return
        }
        
        var attr = [String:AnyObject]()
        if let font = font {
            attr[NSFontAttributeName] = font
        }
        if let color = color {
            attr[NSForegroundColorAttributeName] = color
        }
        
        let aStr = NSAttributedString(string: title, attributes: attr)
        
        self.setAttributedTitle(aStr, forState: state)
        
    }
}

