//
//  PHAlertControllerAlertActionButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

internal class PHAlertControllerAlertActionButton : PHAlertControllerActionButton {
    
    override var minimumIntrinsicContentSize: CGSize {
        return CGSize(width: 0.0, height: 44.0)
    }
    
}
