//
//  PHAlertControllerDefaultButton.swift
//  PHAlertController
//
//  Created by Davor Bauk on 2015-12-16.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit

class PHAlertControllerDefaultButton : PHAlertControllerButton {
    
    override func setup() {
        
        super.setup()
        
        self.setBackgroundColor(UIColor.whiteColor().colorWithAlphaComponent(0.5), forState: .Normal)
        
        self.setBackgroundColor(UIColor.whiteColor().colorWithAlphaComponent(0.1), forState: .Highlighted)
    }
}
