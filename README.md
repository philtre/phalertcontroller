# PHAlertController

[![CI Status](http://img.shields.io/travis/philtre/PHAlertController.svg?style=flat)](https://travis-ci.org/philtre/PHAlertController)
[![Version](https://img.shields.io/cocoapods/v/PHAlertController.svg?style=flat)](http://cocoapods.org/pods/PHAlertController)
[![License](https://img.shields.io/cocoapods/l/PHAlertController.svg?style=flat)](http://cocoapods.org/pods/PHAlertController)
[![Platform](https://img.shields.io/cocoapods/p/PHAlertController.svg?style=flat)](http://cocoapods.org/pods/PHAlertController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PHAlertController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PHAlertController"
```

## Author

philtre, philtre@gmail.com

## License

PHAlertController is available under the MIT license. See the LICENSE file for more info.
