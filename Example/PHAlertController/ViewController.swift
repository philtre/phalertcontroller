//
//  ViewController.swift
//  PHAlertControllerDemo
//
//  Created by Davor Bauk on 2015-12-10.
//  Copyright © 2015 Davor Bauk. All rights reserved.
//

import UIKit
import MapKit
import PHAlertController

class ViewController: UIViewController {

    
    var mapView:MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.greenColor()
        
        self.mapView = MKMapView()
        self.mapView.mapType = .Hybrid
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        
        self.mapView.topAnchor.constraintEqualToAnchor(self.view.topAnchor).active = true
        self.mapView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true
        self.mapView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true
        self.mapView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true
        
        self.mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "onTap:"))
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            print("present")
            self.present()
        }
    }


    @objc private func onTap(gesture:UITapGestureRecognizer) {
        self.present()
    }
    func present() {
        if (self.typeIdx % 4) < 2 {
            self.presentCustomAlert()
        } else {
            self.presentSystemAlert()
        }
//        self.presentCustomDialog()
    }
    func presentSystemAlert()  {
        let typeIdx = (self.typeIdx++)%2
        //        let phStyle = PHAlertControllerStyle.Alert
        //        let phStyle = PHAlertControllerStyle.ActionSheet
        let style:UIAlertControllerStyle = typeIdx == 0 ? .Alert : .ActionSheet
        
        let title:String? = "Title"
        let message:String = "Message"
        let alert = UIAlertController(title: title , message: message, preferredStyle: style)
        alert.addAction(
            UIAlertAction(
                title: "Maybe",
                style: .Destructive,
                handler: { action in print("selected Maybe") }
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "Delete",
                style: .Destructive,
                handler: { action in print("selected Deleted") }
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .Cancel,
                handler: { action in print("selected No") }
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "OK",
                style: .Default,
                handler: { action in print("selected OK") }
            )
        )
        alert.preferredAction = alert.actions[1]
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 40.0, y: 40.0, width: 20.0, height: 20.0)
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    var typeIdx = 0
    func presentCustomAlert() {
        let typeIdx = (self.typeIdx++)%2
//        let phStyle = PHAlertControllerStyle.Alert
//        let phStyle = PHAlertControllerStyle.ActionSheet
        let phStyle:PHAlertControllerStyle = typeIdx == 0 ? .Alert : .ActionSheet
        let title:String? = "Title Title Title TitleTitle Title"
        let message:String? = "Message Message Message Message Message Message"
        let alert = PHAlertController(title: title, message: message, preferredStyle: phStyle)
        
        alert.addAction(
            PHAlertAction(
                title: "OK",
                style: .Default,
                handler: { action in
                    print("selected OK")
                    alert.numpadIntegerValue = 1235678
                }
            )
        )
        alert.addAction(
            PHAlertAction(
                title: "Maybe",
                style: .Default,
                handler: { action in
                    print("selected Maybe")
                    alert.numpadDoubleValue = 123.456789
                }
            )
        )
        alert.addAction(
            PHAlertAction(
                title: "Cancel",
                style: .Cancel,
                handler: { action in print("selected Cancel") }
            )
        )
        alert.addAction(
            PHAlertAction(
                title: "Delete",
                style: .Destructive,
                handler: { action in print("selected Delete") }
            )
        )
        alert.addNumpad()
        alert.preferredAction = alert.actions[1]
        self.presentViewController(alert, animated: true) {
            print("presented")
        }
    }
    func presentCustomDialog() {
/*
        let title:String? = "Title Title Title TitleTitle Title"
        let message:String? = "Message Message Message Message Message Message"
        let alert = PHNumpadAlertController(title: title, message: message)
        
        alert.addAction(
            PHAlertAction(
                title: "Dispatch",
                style: .Default,
                handler: { action in print("selected Dispatch") }
            )
        )
        alert.addAction(
            PHAlertAction(
                title: "Cancel",
                style: .Cancel,
                handler: { action in print("selected Cancel") }
            )
        )
        self.presentViewController(alert, animated: true) {
            print("presented")
        }
*/
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("will disappear")
    }
}

